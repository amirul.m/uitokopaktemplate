// webpack.mix.js

let mix = require('laravel-mix');

mix.sass('assets/sass/custom.scss', 'assets/css/custom.css')
.sass('assets/sass/bootstrap-custom.scss', 'assets/css/bootstrap-custom.css')
.sass('assets/sass/login.scss', 'assets/css/login.css')
.sass('assets/sass/dashboard.scss', 'assets/css/dashboard.css');


// mix.browserSync('uitokopaktemplate.test').disableNotifications();