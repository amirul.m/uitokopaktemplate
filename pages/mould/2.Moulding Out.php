<?php include("../../setrelative.php") ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Moulding Out</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/package/bootstrap-4.5.3/css/bootstrap.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Source+Code+Pro&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/main.css">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/bootstrap-custom.css">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/custom.css">

</head>

<body>
    <div class="container p-0" id="main-page">
        <div class="jumbotron p-1">
            <div class="page-title page-title-center">
                <h3>Scan Out Mould</h3>
            </div>
            
            <div class="row page-section justify-content-center">
                <div class="col-md-10 col-lg-8">
                    <form>
                        <div class="input-group">
                            <div class="input-group-prepend prepend-30">
                                <span class="input-group-text">Reason</span>
                            </div>
                            <select class="form-control" id="">
                            <option>PRODUCTION</option>
                            <option>REPAIR</option>
                            <option>TO ISM</option>
                            <option>TO THP</option>
                            <option>TO TPBP</option>
                            <option>TO TPPG</option>
                            <option>TO TPS</option>
                            <option>OTHERS</option>
                            </select>
                        </div>

                        <div class="page-subtitle page-subtitle-center">
                            <h3>QR CODE</h3>
                        </div>

                        <div class="input-group">
                            <input type="text" class="form-control" id="" placeholder="SCAN QR Code here" autocomplete="off">
                            <div class="input-group-append">
                                <span class="btn btn-outline-primary" data-for=""><i class="fa fa-camera">CAM</i></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="reset" class="btn btn-primary ml-2 float-right" id="">Clear</button>
                        </div>
                    </form>
                </div>
            </div>  
            
            <div class="row page-section justify-content-center">
                <div class="col-md-10 col-lg-8 mb-3">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Col 1</th>
                                    <th>Col 2</th>
                                    <th>Col 3</th>
                                </tr>
                                <tr>
                                    <td class=""><input type="checkbox" name=""></td>
                                    <td class="">001</td>
                                    <td class="">002</td>
                                    <td class="">003</td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <!-- <table class="" id="">
                            <tbody>
                                <tr>
                                    <td class=""><input type="checkbox" name=""></td>
                                    <td class=""></td>
                                    <td class=""></td>
                                    <td class=""></td>
                                </tr>
                            </tbody>
                        </table> -->
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary ml-2 float-right" id="form-submit-btn">Submit</button>
                        <button class="btn btn-primary ml-2 float-right" id="form-delete-btn">Delete</button>
                    </div>
                </div>
            </div>
        </div> 
    </div>
</body>