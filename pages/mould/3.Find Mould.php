<?php include("../../setrelative.php") ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Find Mould</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/package/bootstrap-4.5.3/css/bootstrap.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Source+Code+Pro&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/main.css">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/bootstrap-custom.css">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/custom.css">

</head>

<body>
    <div class="container p-0" id="main-page">
        <div class="jumbotron p-1">
            <div class="page-title page-title-center">
                <h3>Find Mould</h3>
            </div>
            
            <div class="row page-section justify-content-center">
                <div class="col-md-10 col-lg-8">
                    <form>
                        <div class="input-group">
                            <div class="input-group-prepend prepend-30">
                                <span class="input-group-text">Mould NO</span>
                            </div>
                            <select class="form-control" id="">
                            <option>UNBOUND</option>
                            <option>ACTIVE</option>
                            <option>OTHERS</option>
                            </select>
                        </div>

                        <div class="input-group">
                            <div class="input-group-prepend prepend-30">
                                <span class="input-group-text">Status</span>
                            </div>
                            <input type="text" class="form-control" id="" placeholder="Status">
                        </div>

                        <div class="input-group">
                            <div class="input-group-prepend prepend-30">
                                <span class="input-group-text">Bin Location</span>
                            </div>
                            <input type="text" class="form-control" id="" placeholder="Bin Location">
                        </div>

                        <div class="input-group">
                            <div class="input-group-prepend prepend-30">
                                <span class="input-group-text">Reason</span>
                            </div>
                            <input type="text" class="form-control" id="" placeholder="Reason">
                        </div>

                        <div class="form-group">
                            <button type="reset" class="btn btn-primary ml-2 float-right" id="">Clear</button>
                        </div>
                    </form>
                </div>
            </div>  
        </div> 
    </div>
</body>