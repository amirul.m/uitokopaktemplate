<?php include("../../setrelative.php") ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Moulding Condition</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/package/bootstrap-4.5.3/css/bootstrap.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Source+Code+Pro&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/main.css">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/bootstrap-custom.css">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/custom.css">

</head>

<body>
    <div class="container p-0" id="main-page">
        <div class="jumbotron p-1"> 
            <div class="page-title page-title-center">
                <h3>Mould Condition</h3>
            </div>
            <form>
                <div class="row page-section justify-content-center">
                    <div class="col-md-10 col-lg-8"> 
                        <div class="input-group">
                            <div class="input-group-prepend prepend-30">
                                <span class="input-group-text">
                                    Date
                                    <font color="red">*</font>
                                </span>
                            </div>
                            <input type="date" class="form-control" id="t" placeholder="dd/mm/yyyy" value="">
                        </div>

                        <div class="input-group">
                            <div class="input-group-prepend prepend-30">
                                <span class="input-group-text">
                                    Customer Name
                                    <font color="red">*</font>
                                </span>
                            </div>
                            <select class="form-control" id="">
                                <option></option>
                                <option>Daikin</option>
                                <option>PJM</option>
                                <option>YEH BROTHER</option>
                                <option>OTHERS</option>
                            </select>
                        </div>

                        <div class="input-group">
                            <div class="input-group-prepend prepend-30">
                                <span class="input-group-text">
                                    Part No
                                    <font color="red">*</font>
                                </span>
                            </div>
                            <select class="form-control" id="">
                                <option></option>
                                <option>PFPN1467/8YA</option>
                                <option>PJPIC 0024ZA</option>
                            </select>
                        </div>

                        <div class="input-group">
                            <div class="input-group-prepend prepend-30">
                                <span class="input-group-text">
                                    Machine No
                                    <font color="red">*</font>
                                </span>
                            </div>
                            <select class="form-control" id="">
                                <option></option>
                                <option>Machine 1</option>
                                <option>Machine 2</option>
                            </select>
                        </div>
                                               
                        <div class="input-group">
                            <div class="input-group-prepend prepend-30">
                                <span class="input-group-text">
                                    Checking For
                                    <font color="red">*</font>
                                </span>
                            </div>
                            <div class="form-control radio">
                                <input label="Up Mould" type="radio" id="" name="mould" value="Up Mould">
                                <input label="Down Mould" type="radio" id="" name="mould" value="Donw Mould">
                            </div>
                        </div>
                    </div>
                </div>  

                <div class="row page-section justify-content-center">
                    <div class="col-md-10 col-lg-8 mb-3">
                        <div class="page-subtitle page-subtitle-center">
                            <h3>
                                FIXED MOULD
                                <font color="red">*</font>
                            </h3>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered" id="">
                                <thead>
                                    <tr>
                                        <th class="col-custom-1"></th>
                                        <th class="status">Status</th>
                                    </tr>
                                </thead>
                                <tbody> 
                                    <tr>
                                        <td>Corevent Condition</td>
                                        <td class="">
                                            <div class="radio">
                                                <input label="OK" type="radio" id="" name="status-1-1" value="OK">
                                                <input label="NG" type="radio" id="" name="status-1-1" value="NG">
                                            </div>                                     
                                        </td>    
                                    
                                    </tr>
                                    <tr>
                                        <td>Cavity screw all tightened</td>
                                        <td class="">
                                            <div class="radio">
                                                <input label="OK" type="radio" id="" name="status-1-2" value="OK">
                                                <input label="NG" type="radio" id="" name="status-1-2" value="NG">
                                            </div>                                     
                                        </td>  
                                    </tr>
                                    <tr>
                                        <td>Mould dimension</td>
                                        <td class="">
                                            <div class="radio">
                                                <input label="OK" type="radio" id="" name="status-1-3" value="OK">
                                                <input label="NG" type="radio" id="" name="status-1-3" value="NG">
                                            </div>                                   
                                        </td>  
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="page-subtitle page-subtitle-center">
                            <h3>
                                MOVED MOULD
                                <font color="red">*</font>
                            </h3>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered" id="">
                                <thead>
                                    <tr>
                                        <th class="col-custom-1"></th>
                                        <th class="status">Status</th>
                                    </tr>                          
                                </thead>
                                <tbody> 
                                    <tr>
                                        <td >Corevent Condition</td>
                                        <td class="">
                                            <div class="radio">
                                                <input label="OK" type="radio" id="" name="status-2-1" value="OK">
                                                <input label="NG" type="radio" id="" name="status-2-1" value="NG">
                                            </div>                                     
                                        </td>    
                                    </tr>
                                    <tr>
                                        <td>Cavity screw all tightened</td>
                                        <td class="">
                                            <div class="radio">
                                                <input label="OK" type="radio" id="" name="status-2-2" value="OK">
                                                <input label="NG" type="radio" id="" name="status-2-2" value="NG">
                                            </div>                                     
                                        </td>    
                                    </tr>
                                    <tr>
                                        <td>Mould dimension</td>
                                        <td class="">
                                            <div class="radio">
                                                <input label="OK" type="radio" id="" name="status-2-3" value="OK">
                                                <input label="NG" type="radio" id="" name="status-2-3" value="NG">
                                            </div>                                     
                                        </td>    
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="page-subtitle page-subtitle-center">
                            <h3>
                                CHAMBER & EXTENSION
                                <font color="red">*</font>
                            </h3>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered" id="">
                                <thead>
                                    <tr>
                                        <th class="col-custom-1"></th>
                                        <th class="status">Status</th>
                                    </tr>
                                </thead>
                                <tbody> 
                                    <tr>
                                        <td>Chamber dimension</td>
                                        <td class="">
                                            <div class="radio">
                                                <input label="OK" type="radio" id="" name="status-3-1" value="OK">
                                                <input label="NG" type="radio" id="" name="status-3-1" value="NG">
                                            </div>                                     
                                        </td> 
                                    </tr>
                                    <tr>
                                        <td>Chamber screw condition</td>
                                        <td class="">
                                            <div class="radio">
                                                <input label="OK" type="radio" id="" name="status-3-2" value="OK">
                                                <input label="NG" type="radio" id="" name="status-3-2" value="NG">
                                            </div>                                     
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Extension dimension</td>
                                        <td class="">
                                            <div class="radio">
                                                <input label="OK" type="radio" id="" name="status-3-3" value="OK">
                                                <input label="NG" type="radio" id="" name="status-3-3" value="NG">
                                            </div>                                     
                                        </td> 
                                    </tr>
                                    <tr>
                                        <td>Sealing condition of Chamber and Extension</td>
                                        <td class="">
                                            <div class="radio">
                                                <input label="OK" type="radio" id="" name="status-3-4" value="OK">
                                                <input label="NG" type="radio" id="" name="status-3-4" value="NG">
                                            </div>                                     
                                        </td> 
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="page-subtitle page-subtitle-center">
                            <h3>
                                WATER PLATE
                                <font color="red">*</font>
                            </h3>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered" id="">
                                <thead>
                                    <tr>
                                        <th class="col-custom-1"></th>
                                        <th class="status">Status</th>
                                    </tr>                     
                                </thead>
                                <tbody> 
                                    <tr>
                                        <td class="">Condition of Supporting Bar</td>
                                        <td class="">
                                            <div class="radio">
                                                <input label="OK" type="radio" id="" name="status-4-1" value="OK">
                                                <input label="NG" type="radio" id="" name="status-4-1" value="NG">
                                            </div>                                     
                                        </td> 
                                    </tr>
                                    <tr>
                                        <td>Condition of Ejector Bush O-Ring</td>
                                        <td class="">
                                            <div class="radio">
                                                <input label="OK" type="radio" id="" name="status-4-2" value="OK">
                                                <input label="NG" type="radio" id="" name="status-4-2" value="NG">
                                            </div>                                     
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Condition of Spray Nozzle</td>
                                        <td class="">
                                            <div class="radio">
                                                <input label="OK" type="radio" id="" name="status-4-3" value="OK">
                                                <input label="NG" type="radio" id="" name="status-4-3" value="NG">
                                            </div>                                     
                                        </td> 
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="page-subtitle page-subtitle-center">
                            <h3>
                                COOLING PIPE
                                <font color="red">*</font>
                            </h3>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered" id="">
                                <thead>
                                    <tr>
                                        <th class="col-custom-1"></th>
                                        <th class="status">Status</th>
                                    </tr>       
                                </thead>
                                <tbody> 
                                    <tr>
                                        <td class="">Condition of Water Spray Nozzle</td>
                                        <td class="">
                                            <div class="radio">
                                                <input label="OK" type="radio" id="" name="status-5-1" value="OK">
                                                <input label="NG" type="radio" id="" name="status-5-1" value="NG">
                                            </div>                                     
                                        </td> 
                                    </tr>
                                    <tr>
                                        <td>Cooling pipe in correct angle and position</td>
                                        <td class="">
                                            <div class="radio">
                                                <input label="OK" type="radio" id="" name="status-5-2" value="OK">
                                                <input label="NG" type="radio" id="" name="status-5-2" value="NG">
                                            </div>                                     
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                
                        <div class="page-subtitle page-subtitle-center">
                            <h3>
                                FEEDING GUN
                                <font color="red">*</font>
                            </h3>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered" id="">
                                <thead>
                                    <tr>
                                        <th class="col-custom-1"></th>
                                        <th class="status">Status</th>
                                    </tr>
                                </thead>
                                <tbody> 
                                    <tr>
                                        <td class="">Condition of Air Tubing</td>
                                        <td class="">
                                            <div class="radio">
                                                <input label="OK" type="radio" id="" name="status-6-1" value="OK">
                                                <input label="NG" type="radio" id="" name="status-6-1" value="NG">
                                            </div>                                     
                                        </td> 
                                    </tr>
                                    <tr>
                                        <td>Condition of Air FItting</td>
                                        <td class="">
                                            <div class="radio">
                                                <input label="OK" type="radio" id="" name="status-6-2" value="OK">
                                                <input label="NG" type="radio" id="" name="status-6-2" value="NG">
                                            </div>                                     
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
            
                        <div class="page-subtitle page-subtitle-center">
                            <h3>
                                EJECTOR
                                <font color="red">*</font>
                            </h3>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered" id="">
                                <thead>
                                    <tr>
                                        <th class="col-custom-1"></th>
                                        <th class="status">Status</th>
                                    </tr>                
                                </thead>
                                <tbody> 
                                    <tr>
                                        <td >Spindle</td>
                                        <td class="">
                                            <div class="radio">
                                                <input label="OK" type="radio" id="" name="status-7-1" value="OK">
                                                <input label="NG" type="radio" id="" name="status-7-1" value="NG">
                                            </div>                                     
                                        </td> 
                                    </tr>
                                    <tr>
                                        <td>Screw and Nut</td>
                                        <td class="">
                                            <div class="radio">
                                                <input label="OK" type="radio" id="" name="status-7-2" value="OK">
                                                <input label="NG" type="radio" id="" name="status-7-2" value="NG">
                                            </div>                                     
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Spring dimension</td>
                                        <td class="">
                                            <div class="radio">
                                                <input label="OK" type="radio" id="" name="status-7-3" value="OK">
                                                <input label="NG" type="radio" id="" name="status-7-3" value="NG">
                                            </div>                                     
                                        </td> 
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="page-subtitle page-subtitle-center">
                            <h3>
                                CLEANLINESS
                                <font color="red">*</font>
                            </h3>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered" id="">
                                <thead>
                                    <tr>
                                        <th class="col-custom-1"></th>
                                        <th class="status">Status</th>
                                    </tr>
                                </thead>
                                <tbody> 
                                    <tr>
                                        <td>Mould Cleaning</td>
                                        <td class="">
                                            <div class="radio">
                                                <input label="OK" type="radio" id="" name="status-8-1" value="OK">
                                                <input label="NG" type="radio" id="" name="status-8-1" value="NG">
                                            </div>                                     
                                        </td> 
                                    </tr>
                                    <tr>
                                        <td>Mould returned to designated rack</td>
                                        <td class="">
                                            <div class="radio">
                                                <input label="OK" type="radio" id="" name="status-8-2" value="OK">
                                                <input label="NG" type="radio" id="" name="status-8-2" value="NG">
                                            </div>                                     
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="input-group">
                            <div class="input-group-prepend prepend-30">
                                <span class="input-group-text">Comment</span>
                            </div>
                            <textarea class="form-control" id="comment-textarea" placeholder="Comment..."></textarea>
                        </div>
                        <div class="form-group">
                            <button type="reset" class="btn btn-primary ml-2 float-right" id="">Submit</button>
                        </div>
                    </div>
                </div>
            </form>
            
        </div> 
    </div>
</body>