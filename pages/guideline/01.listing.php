<?php include("../../setrelative.php") ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Listing Page</title>


    <?php include(RelativePath."/include_packagecss.php") ?>
    <?php include(RelativePath."/include_assetscss.php") ?>
</head>
<body>
    <div class="container-fluid my-3">
        <div class="jumbotron jumbotron-background">
            <h1>Listing page</h1>
            <h3>This is the page where display all data</h3>
            <p>First thing first must add in title (may refer <a href="../pages/00.basic.php#3-title">Basic page</a>)</p>
            <p>It consists of 2 section, Searching Section and Listing Section.</p>


            <hr id="1-jumbotron">
            <h3>#1: Initial Setup</h3>
            <p>Before start anything, after the <code>body</code> element must include <code>div</code> element with <code>.container-fluid</code>. Then, put <code>div</code> element with <code>.jumbotron .jumbotron-master</code>.</p>
            <p>Then, include the title (may refer <a href="../pages/00.basic.php#3-title">Basic page</a>).</p>

            <div id="code-display">
                <div class="container-fluid">
                    <div class="jumbotron jumbotron-master div-display">
                        <div class="page-title">
                            <h3>div .jumbotron</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div id="code-snippet">
                <textarea class="form-control">
<div class="container-fluid">
    <div class="jumbotron jumbotron-master">
        <div class="page-title">
            <h3>div .jumbotron</h3>
        </div>
    </div>
</div></textarea>
                <button type="button" class="btn-clipboard" id="button-copy">Copy</button>
                <button type="button" class="btn-toggle" id="button-toggle">Toggle</button>
            </div>

            <hr id="2-search">
            <h3>#2: Searching Section</h3>
            <p>This section will allow user to filter out the listing data.</p>
            <p>First, <code>div</code> element with <code>.row .page-section</code>.</p>
            <p>Secondly, the layout for this section will be 4 column meaning that <code>div</code> element with <code>.col-md-4</code>.</p>
            <p>Each of the column will be the field input, may refer to <a href="../pages/04.component.php#1-group">Component page</a></p>
            <p>After that, put <code>div</code> element with <code>.col-12</code> on the last because we will add in button for this column.</p>
            
            <div id="code-display">
                <div class="container-fluid">
                    <div class="row page-section">
                        <div class="col-md-4 div-display">
                            div .col-md-4
                        </div>
                        <div class="col-md-4 div-display">
                            div .col-md-4
                        </div>
                        <div class="col-md-4 div-display">
                            div .col-md-4
                        </div>
                        <div class="col-12 div-display">
                            div .col-12 (For the button)
                        </div>
                    </div>
                </div>
            </div>

            <div id="code-snippet">
                <textarea class="form-control">
<div class="row page-section">
    <div class="col-md-4">
        div .col-md-4
    </div>
    <div class="col-md-4">
        div .col-md-4
    </div>
    <div class="col-md-4">
        div .col-md-4
    </div>
    <div class="col-12">
        div .col-12 (For the button)
    </div>
</div></textarea>
                <button type="button" class="btn-clipboard" id="button-copy">Copy</button>
                <button type="button" class="btn-toggle" id="button-toggle">Toggle</button>
            </div>

            <p>As for the button, we will have Search button.</p>
            <p>Inside the <code>div</code> element with <code>.col-12</code>, put <code>div</code> element with <code>.form-group .mb-0</code> then add two button.</p>
            <p>The New button will be <code>button</code> element with <code>.btn .btn-primary .mr-2</code></p>
            <p>The Search button will be <code>button</code> element with <code>.btn .btn-primary .ml-2 .float-right</code></p>
            <p>The <code>.float-right</code> will make the button stay on the right side</p>
            
            <div id="code-display">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12 div-display">
                            <div class="form-group mb-0">
                                <button class="btn btn-primary mr-2"> 
                                    New
                                </button>
                                <button class="btn btn-primary ml-2 float-right"> 
                                    Search
                                </button>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>

            <div id="code-snippet">
                <textarea class="form-control">
<div class="col-12">
    <div class="form-group mb-0">
        <button class="btn btn-primary mr-2"> 
            New
        </button>
        <button class="btn btn-primary ml-2 float-right"> 
            Search
        </button>
    </div> 
</div></textarea>
                <button type="button" class="btn-clipboard" id="button-copy">Copy</button>
                <button type="button" class="btn-toggle" id="button-toggle">Toggle</button>
            </div>


            <hr id="3-listing">
            <h3>#3: Listing Section</h3>
            <p>This section will list out all data.</p>
            <p>First, <code>div</code> element with <code>.row .page-section</code> and then <code>div</code> element with <code>.col-12</code>.</p></p>
            <p>For now, will use basic <code>table</code> element with <code>.table .table-bordered</code>, later will have enhancement. You may follow as below code:</p>
            
            <div id="code-display">
                <div class="container-fluid">
                    <div class="row page-section">
                        <div class="col-12">
                            <table class="table table-bordered mb-0">
                                <thead>
                                    <tr>
                                        <th>th element</th>
                                        <th>th element</th>
                                        <th>th element</th>
                                        <th>th element</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>td element</td>
                                        <td>td element</td>
                                        <td>td element</td>
                                        <td>td element</td>
                                    </tr>
                                    <tr>
                                        <td>td element</td>
                                        <td>td element</td>
                                        <td>td element</td>
                                        <td>td element</td>
                                    </tr>
                                    <tr>
                                        <td>td element</td>
                                        <td>td element</td>
                                        <td>td element</td>
                                        <td>td element</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div id="code-snippet">
                <textarea class="form-control">
<div class="row page-section">
    <div class="col-12">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>th element</th>
                    <th>th element</th>
                    <th>th element</th>
                    <th>th element</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>td element</td>
                    <td>td element</td>
                    <td>td element</td>
                    <td>td element</td>
                </tr>
                <tr>
                    <td>td element</td>
                    <td>td element</td>
                    <td>td element</td>
                    <td>td element</td>
                </tr>
                <tr>
                    <td>td element</td>
                    <td>td element</td>
                    <td>td element</td>
                    <td>td element</td>
                </tr>
            </tbody>
        </table>
    </div>
</div></textarea>
                <button type="button" class="btn-clipboard" id="button-copy">Copy</button>
                <button type="button" class="btn-toggle" id="button-toggle">Toggle</button>
            </div>
    </div>
    
    <?php include(RelativePath."/include_mainbutton.php") ?>

    <?php include(RelativePath."/include_packagejs.php") ?>
    <?php include(RelativePath."/include_assetsjs.php") ?>
</body>
</html>