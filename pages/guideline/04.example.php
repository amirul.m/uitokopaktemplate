<?php include("../../setrelative.php") ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Example</title>


    <?php include(RelativePath."/include_packagecss.php") ?>
    <?php include(RelativePath."/include_assetscss.php") ?>
</head>
<body>
    <div class="container-fluid my-3">
        <div class="jumbotron jumbotron-background">
            <h1>Example</h1>
            <h3>This is a example for both Listing Page and Maintenance Page.</h3>

            <hr id="1-listing">
            <h3>#1: Listing Page</h3>
            
            <div id="code-display">
                <div class="container-fluid">
                    <div class="jumbotron jumbotron-master jumbotron-shadow">
                        <div class="page-title">
                            <h3>Listing Page</h3>
                        </div>

                        <div class="row page-section">
                            <div class="col-md-4">
                                <div class="input-group">
                                    <div class="input-group-prepend prepend-30">
                                        <span class="input-group-text">Label</span>
                                    </div>
                                    <input type="text" class="form-control" placeholder="Input">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <div class="input-group-prepend prepend-30">
                                        <span class="input-group-text">Label</span>
                                    </div>
                                    <select class="form-control">
                                        <option value="">Select option</option>
                                        <option value="">Option 1</option>
                                        <option value="">Option 2</option>
                                        <option value="">Option 3</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <div class="input-group-prepend prepend-30">
                                        <span class="input-group-text">Label</span>
                                    </div>
                                    <div class="form-control control-height-auto">
                                        <div class="form-check form-check-inline">
                                            <input type="radio" name="radio_1" class="form-check-input" id="radio11">
                                            <label class="form-check-label" for="radio11">
                                                #1
                                            </label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input type="radio" name="radio_1" class="form-check-input" id="radio12">
                                            <label class="form-check-label" for="radio12">
                                                #2
                                            </label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input type="radio" name="radio_1" class="form-check-input" id="radio13">
                                            <label class="form-check-label" for="radio13">
                                                #3
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group mb-0">
                                    <button class="btn btn-primary mr-2"> 
                                        New
                                    </button>
                                    <button class="btn btn-primary ml-2 float-right"> 
                                        Search
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="row page-section">
                            <div class="col-12">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Header 1</th>
                                            <th>Header 2</th>
                                            <th>Header 3</th>
                                            <th>Header 4</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Row 1 with column 1</td>
                                            <td>Row 1 with column 2</td>
                                            <td>Row 1 with column 3</td>
                                            <td>Row 1 with column 4</td>
                                        </tr>
                                        <tr>
                                            <td>Row 2 with column 1</td>
                                            <td>Row 2 with column 2</td>
                                            <td>Row 2 with column 3</td>
                                            <td>Row 2 with column 4</td>
                                        </tr>
                                        <tr>
                                            <td>Row 3 with column 1</td>
                                            <td>Row 3 with column 2</td>
                                            <td>Row 3 with column 3</td>
                                            <td>Row 3 with column 4</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="code-snippet">
                <textarea class="form-control">
<div class="jumbotron">
    <div class="page-title">
        <h3>Listing Page</h3>
    </div>

    <div class="row page-section">
        <div class="col-md-4">
            <div class="input-group">
                <div class="input-group-prepend prepend-30">
                    <span class="input-group-text">Label</span>
                </div>
                <input type="text" class="form-control" placeholder="Input">
            </div>
        </div>
        <div class="col-md-4">
            <div class="input-group">
                <div class="input-group-prepend prepend-30">
                    <span class="input-group-text">Label</span>
                </div>
                <select class="form-control">
                    <option value="">Select option</option>
                    <option value="">Option 1</option>
                    <option value="">Option 2</option>
                    <option value="">Option 3</option>
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="input-group">
                <div class="input-group-prepend prepend-30">
                    <span class="input-group-text">Label</span>
                </div>
                <div class="form-control control-height-auto">
                    <div class="form-check form-check-inline">
                        <input type="radio" name="radio_1" class="form-check-input" id="radio11">
                        <label class="form-check-label" for="radio11">
                            #1
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input type="radio" name="radio_1" class="form-check-input" id="radio12">
                        <label class="form-check-label" for="radio12">
                            #2
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input type="radio" name="radio_1" class="form-check-input" id="radio13">
                        <label class="form-check-label" for="radio13">
                            #3
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="form-group mb-0">
                <button class="btn btn-primary mr-2"> 
                    New
                </button>
                <button class="btn btn-primary ml-2 float-right"> 
                    Search
                </button>
            </div>
        </div>
    </div>
    <div class="row page-section">
        <div class="col-12">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Header 1</th>
                        <th>Header 2</th>
                        <th>Header 3</th>
                        <th>Header 4</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Row 1 with column 1</td>
                        <td>Row 1 with column 2</td>
                        <td>Row 1 with column 3</td>
                        <td>Row 1 with column 4</td>
                    </tr>
                    <tr>
                        <td>Row 2 with column 1</td>
                        <td>Row 2 with column 2</td>
                        <td>Row 2 with column 3</td>
                        <td>Row 2 with column 4</td>
                    </tr>
                    <tr>
                        <td>Row 3 with column 1</td>
                        <td>Row 3 with column 2</td>
                        <td>Row 3 with column 3</td>
                        <td>Row 3 with column 4</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div></textarea>
                <button type="button" class="btn-clipboard" id="button-copy">Copy</button>
                <button type="button" class="btn-toggle" id="button-toggle">Toggle</button>
            </div>

            <hr id="2-maintenance">
            <h3>#2: Maintenance Page</h3>

            <div id="code-display">
                <div class="container-fluid">
                    <div class="jumbotron jumbotron-master jumbotron-shadow">
                        <div class="page-title">
                            <h3>Maintenance Page</h3>
                        </div>
                        <div class="row page-section">
                            <div class="col-md-6">
                                <div class="input-group">
                                    <div class="input-group-prepend prepend-20">
                                        <span class="input-group-text">Label</span>
                                    </div>
                                    <input type="text" class="form-control" placeholder="Input">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <div class="input-group-prepend prepend-20">
                                        <span class="input-group-text">Label</span>
                                    </div>
                                    <input type="text" class="form-control" placeholder="Input">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <div class="input-group-prepend prepend-20">
                                        <span class="input-group-text">Label</span>
                                    </div>
                                    <input type="text" class="form-control control-50" placeholder="Input">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <div class="input-group-prepend prepend-20">
                                        <span class="input-group-text">Label</span>
                                    </div>
                                    <input type="text" class="form-control" placeholder="Input">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <div class="input-group-prepend prepend-20">
                                        <span class="input-group-text">Label</span>
                                    </div>
                                    <select class="form-control">
                                        <option value="">This is select element with .form-control</option>
                                        <option value="">Option 1</option>
                                        <option value="">Option 2</option>
                                        <option value="">Option 3</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <div class="input-group-prepend prepend-20">
                                        <span class="input-group-text">Label</span>
                                    </div>
                                    <textarea class="form-control" row="10"></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <div class="input-group-prepend prepend-20">
                                        <span class="input-group-text">Label</span>
                                    </div>
                                    <div class="form-control control-height-auto">
                                        <div class="form-check form-check-inline">
                                            <input type="checkbox" class="form-check-input" id="checkbox1">
                                            <label class="form-check-label" for="checkbox1">
                                                Checkbox input #1
                                            </label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input type="checkbox" class="form-check-input" id="checkbox2">
                                            <label class="form-check-label" for="checkbox2">
                                                Checkbox input #2
                                            </label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input type="checkbox" class="form-check-input" id="checkbox3">
                                            <label class="form-check-label" for="checkbox3">
                                                Checkbox input #3
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <div class="input-group-prepend prepend-20">
                                        <span class="input-group-text">Label</span>
                                    </div>
                                    <div class="form-control control-height-auto">
                                        <div class="form-check form-check-inline">
                                            <input type="radio" name="radio_2" class="form-check-input" id="radio21">
                                            <label class="form-check-label" for="radio21">
                                                Radio input #1
                                            </label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input type="radio" name="radio_2" class="form-check-input" id="radio22">
                                            <label class="form-check-label" for="radio22">
                                                Radio input #2
                                            </label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input type="radio" name="radio_2" class="form-check-input" id="radio23">
                                            <label class="form-check-label" for="radio23">
                                                Radio input #3
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <button class="btn btn-primary ml-2 float-right"> 
                                        Cancel
                                    </button>
                                    <button class="btn btn-primary ml-2 float-right"> 
                                        Delete
                                    </button>
                                    <button class="btn btn-primary ml-2 float-right"> 
                                        Submit
                                    </button>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="code-snippet">
                <textarea class="form-control">
<div class="jumbotron jumbotron-master">
    <div class="page-title">
        <h3>Maintenance Page</h3>
    </div>
    <div class="row page-section">
        <div class="col-md-6">
            <div class="input-group">
                <div class="input-group-prepend prepend-20">
                    <span class="input-group-text">Label</span>
                </div>
                <input type="text" class="form-control" placeholder="Input">
            </div>
        </div>
        <div class="col-md-6">
            <div class="input-group">
                <div class="input-group-prepend prepend-20">
                    <span class="input-group-text">Label</span>
                </div>
                <input type="text" class="form-control" placeholder="Input">
            </div>
        </div>
        <div class="col-md-6">
            <div class="input-group">
                <div class="input-group-prepend prepend-20">
                    <span class="input-group-text">Label</span>
                </div>
                <input type="text" class="form-control control-50" placeholder="Input">
            </div>
        </div>
        <div class="col-md-6">
            <div class="input-group">
                <div class="input-group-prepend prepend-20">
                    <span class="input-group-text">Label</span>
                </div>
                <input type="text" class="form-control" placeholder="Input">
            </div>
        </div>
        <div class="col-md-6">
            <div class="input-group">
                <div class="input-group-prepend prepend-20">
                    <span class="input-group-text">Label</span>
                </div>
                <select class="form-control">
                    <option value="">This is select element with .form-control</option>
                    <option value="">Option 1</option>
                    <option value="">Option 2</option>
                    <option value="">Option 3</option>
                </select>
            </div>
        </div>
        <div class="col-md-6">
            <div class="input-group">
                <div class="input-group-prepend prepend-20">
                    <span class="input-group-text">Label</span>
                </div>
                <textarea class="form-control" row="10"></ textarea>
            </div>
        </div>
        <div class="col-md-6">
            <div class="input-group">
                <div class="input-group-prepend prepend-20">
                    <span class="input-group-text">Label</span>
                </div>
                <div class="form-control control-height-auto">
                    <div class="form-check form-check-inline">
                        <input type="checkbox" class="form-check-input" id="checkbox1">
                        <label class="form-check-label" for="checkbox1">
                            Checkbox input #1
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input type="checkbox" class="form-check-input" id="checkbox2">
                        <label class="form-check-label" for="checkbox2">
                            Checkbox input #2
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input type="checkbox" class="form-check-input" id="checkbox3">
                        <label class="form-check-label" for="checkbox3">
                            Checkbox input #3
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="input-group">
                <div class="input-group-prepend prepend-20">
                    <span class="input-group-text">Label</span>
                </div>
                <div class="form-control control-height-auto">
                    <div class="form-check form-check-inline">
                        <input type="radio" name="radio_2" class="form-check-input" id="radio21">
                        <label class="form-check-label" for="radio21">
                            Radio input #1
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input type="radio" name="radio_2" class="form-check-input" id="radio22">
                        <label class="form-check-label" for="radio22">
                            Radio input #2
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input type="radio" name="radio_2" class="form-check-input" id="radio23">
                        <label class="form-check-label" for="radio23">
                            Radio input #3
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="form-group">
                <button class="btn btn-primary ml-2 float-right"> 
                    Cancel
                </button>
                <button class="btn btn-primary ml-2 float-right"> 
                    Delete
                </button>
                <button class="btn btn-primary ml-2 float-right"> 
                    Submit
                </button>
            </div> 
        </div>
    </div>
</div></textarea>
                <button type="button" class="btn-clipboard" id="button-copy">Copy</button>
                <button type="button" class="btn-toggle" id="button-toggle">Toggle</button>
            </div>

        </div>
    </div>
    
    <?php include(RelativePath."/include_mainbutton.php") ?>

    <?php include(RelativePath."/include_packagejs.php") ?>
    <?php include(RelativePath."/include_assetsjs.php") ?>
</body>
</html>