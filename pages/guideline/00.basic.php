<?php include("../../setrelative.php") ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Basic</title>


    <?php include(RelativePath."/include_packagecss.php") ?>
    <?php include(RelativePath."/include_assetscss.php") ?>
</head>
<body>
    <div class="container-fluid my-3">
        <div class="jumbotron jumbotron-background">
            <h1>First of all!</h1>
            <h3>Must understand basic to create page</h3>
            <p>Pre-requisites: Must understand HTML and CSS</p>
            <p>Please refer to Bootstrap version 4.5 for more understanding <a href="https://getbootstrap.com/docs/4.5/getting-started/introduction/" target="_blank">(Bootstrap Link)</a></p>
            
            <hr id="1-body">
            <h3>#1: Body Element & Defining Row</h3>
            <p>For all pages, after the <code>body</code> element must include <code>div</code> element with <code>.container-fluid</code>.</p>
            <div id="code-snippet">
                <textarea class="form-control">
<body>
    <div class="container-fluid">
    </div>
</body></textarea>
                <button type="button" class="btn-clipboard" id="button-copy">Copy</button>
                <button type="button" class="btn-toggle" id="button-toggle">Toggle</button>
            </div>
            
            <hr id="2-row">
            <h3>#2: Page Layout</h3>
            <p>Note: If the page doesn't need layout, may skip this section.</p>
            <p>The page must include <code>div</code> element with <code>.row</code> and the divided into 2 column or 3 column using <code>div</code> element with <code>.col-md-6</code> or <code>.col-md-4</code> respectively.</p>
            
            <div id="code-display">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6 div-display">
                            div .col-md-6
                        </div>
                        <div class="col-md-6 div-display">
                            div .col-md-6
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 div-display">
                            div .col-md-4
                        </div>
                        <div class="col-md-4 div-display">
                            div .col-md-4
                        </div>
                        <div class="col-md-4 div-display">
                            div .col-md-4
                        </div>
                    </div>
                </div>
            </div>

            <div id="code-snippet">
                <textarea class="form-control">
<div class="row">
    <div class="col-md-6"></div>
    <div class="col-md-6"></div>
</div>
<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4"></div>
    <div class="col-md-4"></div>
</div></textarea>
                <button type="button" class="btn-clipboard" id="button-copy">Copy</button>
                <button type="button" class="btn-toggle" id="button-toggle">Toggle</button>
            </div>
            
            <p>However if the page require more space on the right side, inside <code>div</code> element with <code>.row</code> and following <code>div</code> element <code>.col-md-4</code> first then <code>.col-md-8</code>. If it require more space on the left side, then the class is vice versa.</p>
            
            <div id="code-display">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-4 div-display">
                            div .col-md-4
                        </div>
                        <div class="col-md-8 div-display">
                            div .col-md-8
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 div-display">
                            div .col-md-8
                        </div>
                        <div class="col-md-4 div-display">
                            div .col-md-4
                        </div>
                    </div>
                </div>
            </div>

            <div id="code-snippet">
                <textarea class="form-control">
<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-8"></div>
</div>
<div class="row">
    <div class="col-md-8"></div>
    <div class="col-md-4"></div>
</div></textarea>
                <button type="button" class="btn-clipboard" id="button-copy">Copy</button>
                <button type="button" class="btn-toggle" id="button-toggle">Toggle</button>
            </div>
            
            <p>Then if the page require multiple row, inside <code>div</code> element with <code>.row</code> and following <code>div</code> element <code>.col-12</code>. Thus no need to use a lot of <code>div</code> element with <code>.row</code>.</p>
            
            <div id="code-display">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12 div-display">
                            div .col-12
                        </div>
                        <div class="col-12 div-display">
                            div .col-12
                        </div>
                        <div class="col-12 div-display">
                            div .col-12
                        </div>
                    </div>
                </div>
            </div>

            <div id="code-snippet">
                <textarea class="form-control">
<div class="row">
    <div class="col-12">
        div .col-12
    </div>
    <div class="col-12">
        div .col-12
    </div>
    <div class="col-12">
        div .col-12
    </div>
</div></textarea>
                <button type="button" class="btn-clipboard" id="button-copy">Copy</button>
                <button type="button" class="btn-toggle" id="button-toggle">Toggle</button>
            </div>

            <hr id="3-title">
            <h3>#3: Title</h3>
            <p>Each page must start title first with <code>div</code> element with <code>.page-title</code> and then following with <code>h3</code> element. After the separator will have page content.</p>
            
            <div id="code-display">
                <div class="container-fluid">
                    <div class="page-title">
                        <h3>Title</h3>
                    </div>
                </div>
            </div>

            <div id="code-snippet">
                <textarea class="form-control">
<div class="page-title">
    <h3>Title</h3>
</div></textarea>
                <button type="button" class="btn-clipboard" id="button-copy">Copy</button>
                <button type="button" class="btn-toggle" id="button-toggle">Toggle</button>
            </div>
        </div>
    </div>
    
    <?php include(RelativePath."/include_mainbutton.php") ?>

    <?php include(RelativePath."/include_packagejs.php") ?>
    <?php include(RelativePath."/include_assetsjs.php") ?>
</body>
</html>