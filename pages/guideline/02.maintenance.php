<?php include("../../setrelative.php") ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Maintenance Page</title>


    <?php include(RelativePath."/include_packagecss.php") ?>
    <?php include(RelativePath."/include_assetscss.php") ?>
</head>
<body>
    <div class="container-fluid my-3">
        <div class="jumbotron jumbotron-background">
            <h1>Maintenance page</h1>
            <h3>This is the page where allow user to insert/update/delete data</h3>

            <hr id="1-jumbotron">
            <h3>#1: Initial Setup</h3>
            <p>Before start anything, after the <code>body</code> element must include <code>div</code> element with <code>.container-fluid</code>. Then, put <code>div</code> element with <code>.jumbotron .jumbotron-master</code>.</p>
            <p>Then, include the title (may refer <a href="../pages/00.basic.php#3-title">Basic page</a>).</p>

            <div id="code-display">
                <div class="container-fluid">
                    <div class="jumbotron jumbotron-master div-display">
                        <div class="page-title">
                            <h3>div .jumbotron</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div id="code-snippet">
                <textarea class="form-control">
<div class="container-fluid">
    <div class="jumbotron jumbotron-master">
        <div class="page-title">
            <h3>div .jumbotron</h3>
        </div>
    </div>
</div></textarea>
                <button type="button" class="btn-clipboard" id="button-copy">Copy</button>
                <button type="button" class="btn-toggle" id="button-toggle">Toggle</button>
            </div>
            
            <hr id="2-layout">
            <h3>#2: Page Layout</h3>
            <p>First, <code>div</code> element with <code>.row .page-section</code>.</p>
            <p>For maintenance page, it will be 2 column layout, thus using <code>div</code> element with <code>.col-md-6</code></p>
            <p>Each of the column will be the field input, may refer to <a href="../pages/04.component.php#1-group">Component page</a></p>
            <p>After that, put <code>div</code> element with <code>.col-12</code> on the last because we will add in button for this column.</p>
            
            <div id="code-display">
                <div class="container-fluid">
                    <div class="row page-section">
                        <div class="col-md-6 div-display">
                            div .col-md-6
                        </div>
                        <div class="col-md-6 div-display">
                            div .col-md-6
                        </div>
                        <div class="col-12 div-display">
                            div .col-12 (For the button)
                        </div>
                    </div>
                </div>
            </div>

            <div id="code-snippet">
                <textarea class="form-control">
<div class="row page-section">
    <div class="col-md-6">
        div .col-md-6
    </div>
    <div class="col-md-6">
        div .col-md-6
    </div>
    <div class="col-12">
        div .col-12
    </div>
</div></textarea>
                <button type="button" class="btn-clipboard" id="button-copy">Copy</button>
                <button type="button" class="btn-toggle" id="button-toggle">Toggle</button>
            </div>
            
            <hr id="3-button">
            <h3>#3: Button</h3>
            <p>At the end of page, will be button to allow user to insert/submit/delete the data.</p>
            <p>Inside the <code>div</code> element with <code>.col-12</code>, put <code>div</code> element with <code>.form-group</code> then add three button.</p>
            <p>The Submit button is for user to insert/submit data.</p>
            <p>The Delete button is for user to delete data.</p>
            <p>The Cancel button is for user to redirect back listing page.</p>
            <p>All button will be same <code>button</code> element with <code>.btn .btn-primary .ml-2 .float-right</code></p>

            <div id="code-display">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12 div-display">
                            <div class="form-group
                            ">
                                <button class="btn btn-primary ml-2 float-right"> 
                                    Cancel
                                </button>
                                <button class="btn btn-primary ml-2 float-right"> 
                                    Delete
                                </button>
                                <button class="btn btn-primary ml-2 float-right"> 
                                    Submit
                                </button>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>

            <div id="code-snippet">
                <textarea class="form-control">
<div class="row">
    <div class="col-12">
        <div class="form-group">
            <button class="btn btn-primary ml-2 float-right"> 
                Cancel
            </button>
            <button class="btn btn-primary ml-2 float-right"> 
                Delete
            </button>
            <button class="btn btn-primary ml-2 float-right"> 
                Submit
            </button>
        </div> 
    </div>
</div></textarea>
                <button type="button" class="btn-clipboard" id="button-copy">Copy</button>
                <button type="button" class="btn-toggle" id="button-toggle">Toggle</button>
            </div>
        </div>
    </div>
    
    <?php include(RelativePath."/include_mainbutton.php") ?>

    <?php include(RelativePath."/include_packagejs.php") ?>
    <?php include(RelativePath."/include_assetsjs.php") ?>
</body>
</html>