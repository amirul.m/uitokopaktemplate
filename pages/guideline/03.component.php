<?php include("../../setrelative.php") ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Component Page</title>


    <?php include(RelativePath."/include_packagecss.php") ?>
    <?php include(RelativePath."/include_assetscss.php") ?>
</head>
<body>
    <div class="container-fluid my-3">
        <div class="jumbotron jumbotron-background">
            <h1>Component page</h1>
            <h3>This is a template on how to put input field such as textbox, listbox, etc.</h3>

            <hr id="1-group">
            <h3>#1: Input Group</h3>
            <p>To standardize all input, first put <code>div</code> element with <code>.input-group</code> and then <code>div</code> element with <code>.input-group-prepend</code> and inside of it put <code>span</code> element with <code>.input-group-text</code>. This is for the label.</p>
            <p>Next the <code>input</code> must include <code>.form-control</code>.</p>
            <p>Refer below code:</p>

            <div id="code-snippet">
                <textarea class="form-control">
<div class="input-group">
    <div class="input-group-prepend">
        <span class="input-group-text">Label</span>
    </div>
    <input type="text" class="form-control" placeholder="Input">
</div></textarea>
                <button type="button" class="btn-clipboard" id="button-copy">Copy</button>
                <button type="button" class="btn-toggle" id="button-toggle">Toggle</button>
            </div>

            <p>Each of these Input Group must put inside each column.</p>
            <p>For example below for 3 column, 2 column and 1 column.</p>

            <div id="code-display">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Label</span>
                                </div>
                                <input type="text" class="form-control" placeholder="Input">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Label</span>
                                </div>
                                <input type="text" class="form-control" placeholder="Input">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Label</span>
                                </div>
                                <input type="text" class="form-control" placeholder="Input">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Label</span>
                                </div>
                                <input type="text" class="form-control" placeholder="Input">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Label</span>
                                </div>
                                <input type="text" class="form-control" placeholder="Input">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">This Label will follow the text length</span>
                                </div>
                                <input type="text" class="form-control" placeholder="This Input will stretch to end">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="code-snippet">
                <textarea class="form-control">
<div class="row">
    <!-- Example for three column -->
    <div class="col-md-4">
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text">Label</span>
            </div>
            <input type="text" class="form-control" placeholder="Input">
        </div>
    </div>

    <!-- Example for two column -->
    <div class="col-md-6">
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text">Label</span>
            </div>
            <input type="text" class="form-control" placeholder="Input">
        </div>
    </div>

    <!-- Example for one column -->
    <div class="col-12">
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text">Label</span>
            </div>
            <input type="text" class="form-control" placeholder="Input">
        </div>
    </div>
</div></textarea>
                <button type="button" class="btn-clipboard" id="button-copy">Copy</button>
                <button type="button" class="btn-toggle" id="button-toggle">Toggle</button>
            </div>

            <p>To adjust the label width, may use <code>.prepend-10</code>, <code>.prepend-20</code> and <code>.prepend-30</code>, these will divide the width into 10%, 20%, 30% of the column width respectively.</p>

            <div id="code-display">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="input-group">
                                <div class="input-group-prepend prepend-10">
                                    <span class="input-group-text">Label</span>
                                </div>
                                <input type="text" class="form-control" placeholder="Input">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="input-group">
                                <div class="input-group-prepend prepend-20">
                                    <span class="input-group-text">Label</span>
                                </div>
                                <input type="text" class="form-control" placeholder="Input">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="input-group">
                                <div class="input-group-prepend prepend-30">
                                    <span class="input-group-text">Label</span>
                                </div>
                                <input type="text" class="form-control" placeholder="Input">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="code-snippet">
                <textarea class="form-control">
<div class="input-group">
    <div class="input-group-prepend prepend-10">
        <span class="input-group-text">Label</span>
    </div>
    <input type="text" class="form-control" placeholder="Input">
</div>

<div class="input-group">
    <div class="input-group-prepend prepend-20">
        <span class="input-group-text">Label</span>
    </div>
    <input type="text" class="form-control" placeholder="Input">
</div>

<div class="input-group">
    <div class="input-group-prepend prepend-30">
        <span class="input-group-text">Label</span>
    </div>
    <input type="text" class="form-control" placeholder="Input">
</div></textarea>
                <button type="button" class="btn-clipboard" id="button-copy">Copy</button>
                <button type="button" class="btn-toggle" id="button-toggle">Toggle</button>
            </div>

            <p>While the input's width also adjustable, may use <code>.control-70</code>, <code>.control-50</code> and <code>.control-30</code>, these will divide the width into 70%, 50%, 30% of the column width respectively.</p>

            <div id="code-display">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Label</span>
                                </div>
                                <input type="text" class="form-control control-70" placeholder="Input">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Label</span>
                                </div>
                                <input type="text" class="form-control control-50" placeholder="Input">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Label</span>
                                </div>
                                <input type="text" class="form-control control-30" placeholder="Input">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="code-snippet">
                <textarea class="form-control">
<div class="input-group">
    <div class="input-group-prepend">
        <span class="input-group-text">Label</span>
    </div>
    <input type="text" class="form-control control-70" placeholder="Input">
</div>

<div class="input-group">
    <div class="input-group-prepend">
        <span class="input-group-text">Label</span>
    </div>
    <input type="text" class="form-control control-50" placeholder="Input">
</div>

<div class="input-group">
    <div class="input-group-prepend">
        <span class="input-group-text">Label</span>
    </div>
    <input type="text" class="form-control control-30" placeholder="Input">
</div></textarea>
                <button type="button" class="btn-clipboard" id="button-copy">Copy</button>
                <button type="button" class="btn-toggle" id="button-toggle">Toggle</button>
            </div>
            
            <hr id="2-control">
            <h3>#2: Form Control</h3>
            <p>Textual form controls—like <code>input</code>, <code>select</code>, and <code>textarea</code> —are styled with the <code>.form-control</code>.</p>

            <div id="code-display">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12 mb-3">
                            <input type="text" class="form-control" value="This is input element with .form-control">
                        </div>
                        <div class="col-12 mb-3">
                            <select class="form-control">
                                <option value="">This is select element with .form-control</option>
                                <option value="">Option 1</option>
                                <option value="">Option 2</option>
                                <option value="">Option 3</option>
                            </select>
                        </div>
                        <div class="col-12">
                            <textarea class="form-control">This is textarea element with .form-control</textarea>
                        </div>
                    </div>
                </div>
            </div>

            <div id="code-snippet">
                <textarea class="form-control">
<!-- Example for input element -->
<input type="text" class="form-control" placeholder="This is input element with .form-control">

<!-- Example for select element -->
<select class="form-control">
    <option value="">This is select element with .form-control</option>
    <option value="">Option 1</option>
    <option value="">Option 2</option>
    <option value="">Option 3</option>
</select>

<!-- Example for textarea element -->
<textarea class="form-control">
This is textarea element with .form-control
</ textarea></textarea>
                <button type="button" class="btn-clipboard" id="button-copy">Copy</button>
                <button type="button" class="btn-toggle" id="button-toggle">Toggle</button>
            </div>

            <p>As for <code>input</code> element with <code>checbox</code> and <code>radio</code> type using different approach.</p>
            <p>First put <code>div</code> element with <code>.form-control</code> and then <code>div</code> element with <code>.form-check .form-check-inline</code></p>
            <p>Inside it, can put <code>checkbox</code> and <code>radio</code> with <code>.form-check-input</code> and then <code>label</code> element with <code>.form-check-label</code></p>
            <p>If using multiple <code>checkbox</code> and <code>radio</code>, all together put inside <code>div</code> element with <code>.form-control .control-height-auto</code>.</p>
            <p>Checkboxes and radios use are built to support HTML-based form validation and provide concise, accessible labels. As such, our <code>input</code>s and <code>label</code>s are sibling elements as opposed to an <code>input</code> within a <code>label</code>. This is slightly more verbose as you must specify id and for attributes to relate the <code>input</code> and <code>label</code>.</p>

            <div id="code-display">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12 mb-3">
                            <div class="form-control control-height-auto">
                                <div class="form-check form-check-inline">
                                    <input type="checkbox" name="checkboxInput" class="form-check-input" id="checkbox1">
                                    <label class="form-check-label" for="checkbox1">
                                        Checkbox input #1
                                    </label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input type="checkbox" name="checkboxInput" class="form-check-input" id="checkbox2">
                                    <label class="form-check-label" for="checkbox2">
                                        Checkbox input #2
                                    </label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input type="checkbox" name="checkboxInput" class="form-check-input" id="checkbox3">
                                    <label class="form-check-label" for="checkbox3">
                                        Checkbox input #3
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 mb-3">
                            <div class="form-control control-height-auto">
                                <div class="form-check form-check-inline">
                                    <input type="radio" name="radioInput" class="form-check-input" id="radio1">
                                    <label class="form-check-label" for="radio1">
                                        Radio input #1
                                    </label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input type="radio" name="radioInput" class="form-check-input" id="radio2">
                                    <label class="form-check-label" for="radio2">
                                        Radio input #2
                                    </label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input type="radio" name="radioInput" class="form-check-input" id="radio3">
                                    <label class="form-check-label" for="radio3">
                                        Radio input #3
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="code-snippet">
                <textarea class="form-control">
<!-- Example for checbox input element -->
<div class="form-control control-height-auto">
    <div class="form-check form-check-inline">
        <input type="checkbox" class="form-check-input" id="checkbox1">
        <label class="form-check-label" for="checkbox1">
            Checkbox input #1
        </label>
    </div>
    <div class="form-check form-check-inline">
        <input type="checkbox" class="form-check-input" id="checkbox2">
        <label class="form-check-label" for="checkbox2">
            Checkbox input #2
        </label>
    </div>
    <div class="form-check form-check-inline">
        <input type="checkbox" class="form-check-input" id="checkbox3">
        <label class="form-check-label" for="checkbox3">
            Checkbox input #3
        </label>
    </div>
</div>

<!-- Example for radio input element -->
<div class="form-control control-height-auto">
    <div class="form-check form-check-inline">
        <input type="radio" class="form-check-input" id="radio1">
        <label class="form-check-label" for="radio1">
            Radio input #1
        </label>
    </div>
    <div class="form-check form-check-inline">
        <input type="radio" class="form-check-input" id="radio2">
        <label class="form-check-label" for="radio2">
            Radio input #2
        </label>
    </div>
    <div class="form-check form-check-inline">
        <input type="radio" class="form-check-input" id="radio3">
        <label class="form-check-label" for="radio3">
            Radio input #3
        </label>
    </div>
</div></textarea>
                <button type="button" class="btn-clipboard" id="button-copy">Copy</button>
                <button type="button" class="btn-toggle" id="button-toggle">Toggle</button>
            </div>

            <hr id="3-button">
            <h3>#3: Button</h3>
            <p>As for button component, we will follow Bootstrap standard <a href="https://getbootstrap.com/docs/4.5/components/buttons/" target="_blank">(Bootstrap Link)</a>.</p>
            
            <p>We will use <code>button</code> element with <code>.btn .btn-primary</code>.</p>

            <div id="code-display">
                <div class="container-fluid">
                    <button class="btn btn-primary" type="submit">Button</button>
                </div>
            </div>

            <div id="code-snippet">
                <textarea class="form-control"><button class="btn btn-primary" type="submit">Button</button></textarea>
                <button type="button" class="btn-clipboard" id="button-copy">Copy</button>
                <button type="button" class="btn-toggle" id="button-toggle">Toggle</button>
            </div>
            
            <p>For styling the button, can add in class as shown below:</p>
            <ul>
                <li><code>.btn-primary</code> for styling using primary color</li>
                <li><code>.btn-secondary</code> for styling using secondary color</li>
                <li><code>.btn-success</code> for styling using success color</li>
                <li><code>.btn-danger</code> for styling using danger color</li>
                <li><code>.btn-warning</code> for styling using warning color</li>
                <li><code>.btn-info</code> for styling using info color</li>
                <li><code>.btn-link</code> for styling using link color</li>
            </ul>

            <div id="code-display">
                <div class="container-fluid">
                    <button class="btn btn-primary" type="submit">Primary</button>
                    <button class="btn btn-secondary" type="submit">Secondary</button>
                    <button class="btn btn-success" type="submit">Success</button>
                    <button class="btn btn-danger" type="submit">Danger</button>
                    <button class="btn btn-warning" type="submit">Warning</button>
                    <button class="btn btn-info" type="submit">Info</button>
                    <button class="btn btn-link" type="submit">Link</button>
                </div>
            </div>

            <div id="code-snippet">
                <textarea class="form-control">
<button class="btn btn-primary" type="submit">Primary</button>

<button class="btn btn-secondary" type="submit">Secondary</button>

<button class="btn btn-success" type="submit">Success</button>

<button class="btn btn-danger" type="submit">Danger</button>

<button class="btn btn-warning" type="submit">Warning</button>

<button class="btn btn-info" type="submit">Info</button>

<button class="btn btn-link" type="submit">Link</button></textarea>
                <button type="button" class="btn-clipboard" id="button-copy">Copy</button>
                <button type="button" class="btn-toggle" id="button-toggle">Toggle</button>
            </div>

        </div>
    </div>
    
    <?php include(RelativePath."/include_mainbutton.php") ?>

    <?php include(RelativePath."/include_packagejs.php") ?>
    <?php include(RelativePath."/include_assetsjs.php") ?>
</body>
</html>