<?php include("../../setrelative.php") ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Use Instascan</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/package/bootstrap-4.5.3/css/bootstrap.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Source+Code+Pro&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/main.css">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/bootstrap-custom.css">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/custom.css">
    <script type="text/javascript" src="../../package/scanner_js/instascan_js/instascan.js"></script>
    <style>
        #preview{
            width: 70%;
            display: none;
            margin: 0 auto;
        }
    </style>
</head>
<body>
    <div class="container-fluid">
        <div class="jumbotron jumbotron-master" >
            <div class="page-title">
                <h3>Instascan</h3>
            </div>
            <div class="row page-section justify-content-center">
                <div class="col-md-6">
                    <div class="input-group">
                        <div class="input-group prepend">
                            <button class="btn btn-secondary btn-block" type="submit" onclick="scan()">
                            CLICK HERE TO SCAN </button> 
                            <button class="btn btn-secondary btn-block" type="submit" onclick="stop()">
                                CLICK HERE TO STOP </button>
                            <button class="btn btn-secondary btn-block" type="submit" id="switch" hidden onclick="swi_tch()">
                                SWITCH CAMERA</button>
                            <h1 id="working"></h1>
                        </div>
                    </div>
                    <video id="preview"></video>
                    <script type="text/javascript">
                            // declare outside to use at scan() and stop()
                            var vid = document.getElementById('preview');
                            var scanner;
                            var result =  document.getElementById('result');
                            var chgCamera = document.getElementById('switch');
                            var workingCamera, workedCamera;

                            function scanProcess(){
                                chgCamera.hidden = false;
                                var result =  document.getElementById('result');
                                result.innerHTML = 'Scanning..';
                                vid.style.display = "block";
                                /* function to scan the code, 
                                    will load the content to variable content
                                */
                                scanner.addListener('scan', function (content) {
                                    result.innerHTML = content;
                                    stop();
                                }); 
                            }
                           
                            function scan(){  
                                chgCamera.hidden = false;    
                                scanner = new Instascan.Scanner({ video: vid});                        
                                Instascan.Camera.getCameras().then( function(cameras) {         
                                    if (cameras.length > 0) {
                                        workedCamera = cameras
                                        workingCamera = 0;
                                        scanner.start(workedCamera[0]);
                                    } else {                                       
                                        console.error('No cameras found.');
                                    }
                                }).catch(function (e) {
                                   
                                    console.error(e);
                                });     
                                scanProcess();           
                            }
                            
                            // the video and the camera will STOP
                            function stop(){
                                chgCamera.hidden = true;
                                vid.style.display="none";
                                scanner.stop();
                            }
                            function swi_tch(){
                                scanner.stop();
                                delete scanner;
                                if(workingCamera == 1){
                                    workingCamera = 0;
                                    scanner = new Instascan.Scanner({ video: vid});
                                }else{
                                    workingCamera = 1;
                                    scanner = new Instascan.Scanner({ video: vid, mirror: false});
                                }
                                scanner.start(workedCamera[workingCamera]);
                                scanProcess();
                            }                    
                    </script>
                    <div class="input-group">
                        <div class="input-group-prepend prepend-80">
                            <span class="input-group-text">Scan Result</span>
                        </div>
                        <textarea class="form-control" id="result">Scan Result display here</textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>