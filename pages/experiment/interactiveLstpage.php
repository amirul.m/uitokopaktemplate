<?php include("../../setrelative.php") ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Interactive Table for Listing Page</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- files for DataTables (interactive table) -->
    <!-- <link rel="stylesheet" href="http://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script> -->

    <link href="//cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="//cdn.datatables.net/buttons/1.5.6/css/buttons.bootstrap4.min.css" rel="stylesheet">

    <script src="//code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="//stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

    <link rel="stylesheet" href="<?php echo RelativePath; ?>/package/bootstrap-4.5.3/css/bootstrap.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Source+Code+Pro&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/main.css">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/bootstrap-custom.css">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/custom.css">

    <!-- Initialise DataTables -->
    <script>
        $(document).ready(function() {
            $('#datatable').DataTable();
        });
    </script>
</head>

<body>
    <div class="container-fluid">
        <div class="jumbotron jumbotron-master">
            <!--Title and navigation bar-->
            <div class="page-title">
                <h3>Interactive Table for Listing Page</h3>
            </div>

            <!--Mstdriver input field-->
            <div class="row page-section">
                <div class="col-md-4">
                    <div class="input-group">
                        <div class="input-group-prepend prepend-30">
                            <span class="input-group-text">Driver Code</span>
                        </div>
                        <input type="text" class="form-control" placeholder="Driver Code">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group">
                        <div class="input-group-prepend prepend-30">
                            <span class="input-group-text">Driver Name</span>
                        </div>
                        <input type="text" class="form-control" placeholder="Driver Name">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group">
                        <div class="input-group-prepend prepend-30">
                            <span class="input-group-text">Driver Category</span>
                        </div>
                        <select class="form-control">
                            <option value="">Local Driver</option>
                            <option value="">Outsource Driver</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group">
                        <div class="input-group-prepend prepend-30">
                            <span class="input-group-text">Transporter</span>
                        </div>
                        <select class="form-control">
                            <option value="">Select option</option>
                            <option value="">Option 1</option>
                            <option value="">Option 2</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group">
                        <div class="input-group-prepend prepend-30">
                            <span class="input-group-text">Active</span>
                        </div>
                        <select class="form-control">
                            <option value="">All</option>
                            <option value="">Active</option>
                            <option value="">Inactive</option>
                        </select>
                    </div>
                </div>
            </div>


            <div class="col-12">
                <div class="form-group mb-0">
                    <button class="btn btn-primary mr-2">
                        New
                    </button>
                    <button class="btn btn-primary ml-2 float-right">
                        Search
                    </button>
                </div>
            </div>

            <!--Mstdriver Listing-->
            <div class="row page-section">
                <div class="col-12">
                    <!-- <table id="datatable" class="table table-bordered display" cellspacing="0" width="100%"> -->
                    <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                        <thead>
                            <tr>
                                <th>Driver Code</th>
                                <th>Driver Name</th>
                                <th>Driver Category</th>
                                <th>Transporter</th>
                                <th>Active</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Row 1 with column 1</td>
                                <td>Row 1 with column 2</td>
                                <td>Row 1 with column 3</td>
                                <td>Row 1 with column 4</td>
                                <td>Row 1 with column 5</td>
                            </tr>
                            <tr>
                                <td>Row 2 with column 1</td>
                                <td>Row 2 with column 2</td>
                                <td>Row 2 with column 3</td>
                                <td>Row 2 with column 4</td>
                                <td>Row 2 with column 5</td>
                            </tr>
                            <tr>
                                <td>Row 3 with column 1</td>
                                <td>Row 3 with column 2</td>
                                <td>Row 3 with column 3</td>
                                <td>Row 3 with column 4</td>
                                <td>Row 3 with column 5</td>
                            </tr>
                            <tr>
                                <td>Row 4 with column 1</td>
                                <td>Row 4 with column 2</td>
                                <td>Row 4 with column 3</td>
                                <td>Row 4 with column 4</td>
                                <td>Row 4 with column 5</td>
                            </tr>
                            <tr>
                                <td>Row 5 with column 1</td>
                                <td>Row 5 with column 2</td>
                                <td>Row 5 with column 3</td>
                                <td>Row 5 with column 4</td>
                                <td>Row 5 with column 5</td>
                            </tr>
                            <tr>
                                <td>Row 6 with column 1</td>
                                <td>Row 6 with column 2</td>
                                <td>Row 6 with column 3</td>
                                <td>Row 6 with column 4</td>
                                <td>Row 6 with column 5</td>
                            </tr>
                            <tr>
                                <td>Row 7 with column 1</td>
                                <td>Row 7 with column 2</td>
                                <td>Row 7 with column 3</td>
                                <td>Row 7 with column 4</td>
                                <td>Row 7 with column 5</td>
                            </tr>
                            <tr>
                                <td>Row 8 with column 1</td>
                                <td>Row 8 with column 2</td>
                                <td>Row 8 with column 3</td>
                                <td>Row 8 with column 4</td>
                                <td>Row 8 with column 5</td>
                            </tr>
                            <tr>
                                <td>Row 9 with column 1</td>
                                <td>Row 9 with column 2</td>
                                <td>Row 9 with column 3</td>
                                <td>Row 9 with column 4</td>
                                <td>Row 9 with column 5</td>
                            </tr>
                            <tr>
                                <td>Row 10 with column 1</td>
                                <td>Row 10 with column 2</td>
                                <td>Row 10 with column 3</td>
                                <td>Row 10 with column 4</td>
                                <td>Row 10 with column 5</td>
                            </tr>
                            <tr>
                                <td>Row 11 with column 1</td>
                                <td>Row 11 with column 2</td>
                                <td>Row 11 with column 3</td>
                                <td>Row 11 with column 4</td>
                                <td>Row 11 with column 5</td>
                            </tr>
                            <tr>
                                <td>Row 12 with column 1</td>
                                <td>Row 12 with column 2</td>
                                <td>Row 12 with column 3</td>
                                <td>Row 12 with column 4</td>
                                <td>Row 12 with column 5</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</body>

</html>