<?php include("../../setrelative.php") ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>QR code generator</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/package/bootstrap-4.5.3/css/bootstrap.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Source+Code+Pro&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/main.css">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/custom.css">


</head>

<body>
    <div class="container-fluid">
        <div class="jumbotron">
            <!--Title and navigation bar-->
            <div class="page-title">
                <h3>Javascript QR Code Generator</h3>
            </div>

            <div class="row page-section">
                <div class="col-md-3">
                    <!-- <div class="input-group">
                        <div class="input-group-prepend prepend-40">
                            <span class="input-group-text">QR Code Generator</span>
                        </div>
                        <input id="qr-text" type="text" class="form-control" placeholder="Enter values" />
                    </div>

                    <div>
                        <button class="btn btn-primary" onclick="generateQRCode()">Create QR Code</button>
                    </div>
                    <br /> -->
                    <!-- <p id="qr-result">This is the QR code generated:</p> -->
                    <p>This is the QR code generated:</p>
                    <canvas id="qr-code"></canvas>

                </div>

            </div>
        </div>
    </div>


    <!-- Using 3rd party Javascript library Qrious -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/qrious/4.0.2/qrious.min.js"></script>
    <script>
        var qr;
        (function() {
            qr = new QRious({
                element: document.getElementById('qr-code'),
                size: 200,
                value: ''
            });

            var qrtext = "121219-B6-02" + "|" + "A7U49762-00A" + "|" + "1900470401";

            qr.set({
                foreground: 'black',
                size: 200,
                value: qrtext
            });
        })();


        //for downloading the qrcode as an image(png)
        var canv = document.getElementsByTagName("canvas")[0],
            image = document.createElement("a");
        image.href = canv.toDataURL();
        image.setAttribute("download", "qrcode_" + (new Date).getTime() + ".png");
        var myEvt = document.createEvent("MouseEvents");
        myEvt.initEvent("click", !1, !0);
        image.dispatchEvent(myEvt);

        // function generateQRCode() {
        //     var qrtext = "hello world";
        //     // var qrtext = document.getElementById("qr-text").value;
        //     // document.getElementById("qr-result").innerHTML = "QR code for " + qrtext + ":";
        //     // alert(qrtext);
        //     qr.set({
        //         foreground: 'black',
        //         size: 200,
        //         value: qrtext
        //     });
        // }
    </script>

</body>

</html>