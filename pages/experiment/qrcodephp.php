<?php include("../../setrelative.php") ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>QR code generator</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- html header to image png -->
    <meta http-equiv="content-type" content="image/png" charset="utf-8">

    <link rel="stylesheet" href="<?php echo RelativePath; ?>/package/bootstrap-4.5.3/css/bootstrap.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Source+Code+Pro&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/main.css">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/custom.css">

</head>

<body>
    <div class="container-fluid">
        <div class="jumbotron">
            <!--Title and navigation bar-->
            <div class="page-title">
                <h3>PHP QR Code Generator</h3>
            </div>

            <div class="row page-section">
                <div class="col-md-3">
                    <p>This is the QR code generated:</p>

                    <!-- php goes here -->
                    <?php
                    // Include the qrlib file 
                    include RelativePath.'/package/phpqrcode/qrlib.php';
                    $text = "121219-B6-02" . "|" . "A7U49762-00A" . "|" . "1900470401";

                  
                    // uniqid creates unique id based on microtime 
                    $path = RelativePath.'/qrimage/';
                    $file = $path . uniqid() . ".png";

                    // $ecc stores error correction capability('L') 
                    $ecc = 'L';

                    // Generates QR Code and Stores it in directory given 
                    QRcode::png($text, $file, $ecc, 7, 2);

                    // Displaying the stored QR code from directory 
                    echo "<img src='" . $file . "'>";
                    ?>


                </div>
            </div>
        </div>
    </div>


</body>

</html>