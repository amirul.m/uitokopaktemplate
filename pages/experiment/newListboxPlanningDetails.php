<?php include("../../setrelative.php") ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Edit Planning Details</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/package/bootstrap-4.5.3/css/bootstrap.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Source+Code+Pro&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/main.css">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/custom.css">


    <!-- Select2 CSS -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!-- Select2 JS -->
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

    <script>
        $(document).ready(function() {
            // Initialize select2
            $(".selectedPart").select2();

            // Retrieve selected option
            // $('.but_read').click(function() {
            //     var username = $('.selectedPart option:selected').text();
            //     var userid = $('.selectedPart').val();

            //     $('.result').html("id : " + userid + ", name : " + username);
            // });
        });
    </script>
</head>

<body>
    <div class="container-fluid">
        <div class="jumbotron">
            <!--Title and navigation bar-->
            <div class="page-title">
                <h3>Edit Planning Details, Machine 1 (New Listbox)</h3>
            </div>

            <div class="row page-section">
                <div class="col-12">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th style="width: 5%">Model</th>
                                <!-- <th style="width: 15%">Part No</th> -->
                                <th>Part No</th>
                                <th style="width: 15%">Description</th>
                                <th style="width: 10%">Type</th>
                                <th style="width: 10%">Shift</th>
                                <th style="width: 10%">Per Pack</th>
                                <th style="width: 10%">Total Cycle</th>
                                <th style="width: 5%">Cavity</th>
                                <th style="width: 10%">Actual Qty</th>
                                <th style="width: 5%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>
                                    <div class="input-group">
                                        <!-- <select class='selectedPart' style='width: 200px;'> -->
                                        <select class="form-control selectedPart">
                                            <option value='0'>Select Part No</option>
                                            <option value='1'>PFPN1467/8YA</option>
                                            <option value='2'>PJPNB0120ZA-B</option>
                                            <option value='3'>PJPNC0013ZA</option>
                                            <option value='4'>PJPNC0260ZA-N</option>
                                            <option value='5'>RPNX1091C-1</option>
                                        </select>
                                        <!-- for displaying the option selected -->
                                        <!-- <input type='button' value='Seleted option' class='but_read'>
                                        <br/>
                                        <div class='result'></div>  -->
                                    </div>
                                </td>
                                <td></td>
                                <td>
                                    <div class="input-group">
                                        <select class="form-control">
                                            <option value="">D-Direct</option>
                                            <option value="">I-Indirect</option>
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <div class="input-group">
                                        <select class="form-control">
                                            <option value="">A</option>
                                            <option value="">B</option>
                                        </select>
                                    </div>
                                </td>
                                <td></td>
                                <td>
                                    <input type="text" class="form-control">
                                </td>
                                <td></td>
                                <td></td>
                                <td>
                                    <button class="btn btn-primary mx-auto d-block">
                                        Save
                                    </button>
                                </td>
                            </tr>


                            <tr>
                                <td>2</td>
                                <td>
                                    <div class="input-group">
                                        <select class="form-control selectedPart">
                                            <option value='0'>Select Part No</option>
                                            <option value='1'>PFPN1467/8YA</option>
                                            <option value='2'>PJPNB0120ZA-B</option>
                                            <option value='3'>PJPNC0013ZA</option>
                                            <option value='4'>PJPNC0260ZA-N</option>
                                            <option value='5'>RPNX1091C-1</option>
                                        </select>
                                    </div>
                                </td>
                                <td></td>
                                <td>
                                    <div class="input-group">
                                        <select class="form-control">
                                            <option value="">D-Direct</option>
                                            <option value="">I-Indirect</option>
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <div class="input-group">
                                        <select class="form-control">
                                            <option value="">A</option>
                                            <option value="">B</option>
                                        </select>
                                    </div>
                                </td>
                                <td></td>
                                <td>
                                    <input type="text" class="form-control">
                                </td>
                                <td></td>
                                <td></td>
                                <td>
                                    <button class="btn btn-primary mx-auto d-block">
                                        Save
                                    </button>
                                </td>
                            </tr>


                            <tr>
                                <td>3</td>
                                <td>
                                    <div class="input-group">
                                        <select class="form-control selectedPart">
                                            <option value='0'>Select Part No</option>
                                            <option value='1'>PFPN1467/8YA</option>
                                            <option value='2'>PJPNB0120ZA-B</option>
                                            <option value='3'>PJPNC0013ZA</option>
                                            <option value='4'>PJPNC0260ZA-N</option>
                                            <option value='5'>RPNX1091C-1</option>
                                        </select>
                                    </div>
                                </td>
                                <td></td>
                                <td>
                                    <div class="input-group">
                                        <select class="form-control">
                                            <option value="">D-Direct</option>
                                            <option value="">I-Indirect</option>
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <div class="input-group">
                                        <select class="form-control">
                                            <option value="">A</option>
                                            <option value="">B</option>
                                        </select>
                                    </div>
                                </td>
                                <td></td>
                                <td>
                                    <input type="text" class="form-control">
                                </td>
                                <td></td>
                                <td></td>
                                <td>
                                    <button class="btn btn-primary mx-auto d-block">
                                        Save
                                    </button>
                                </td>
                            </tr>

                            <tr>
                                <td>4</td>
                                <td>
                                    <select class="form-control selectedPart">
                                        <option value='0'>Select Part No</option>
                                        <option value='1'>PFPN1467/8YA</option>
                                        <option value='2'>PJPNB0120ZA-B</option>
                                        <option value='3'>PJPNC0013ZA</option>
                                        <option value='4'>PJPNC0260ZA-N</option>
                                        <option value='5'>RPNX1091C-1</option>
                                    </select>
                                </td>
                                <td></td>
                                <td>
                                    <div class="input-group">
                                        <select class="form-control">
                                            <option value="">D-Direct</option>
                                            <option value="">I-Indirect</option>
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <div class="input-group">
                                        <select class="form-control">
                                            <option value="">A</option>
                                            <option value="">B</option>
                                        </select>
                                    </div>
                                </td>
                                <td></td>
                                <td>
                                    <input type="text" class="form-control">
                                </td>
                                <td></td>
                                <td></td>
                                <td>
                                    <button class="btn btn-primary mx-auto d-block">
                                        Save
                                    </button>
                                </td>
                            </tr>

                            <tr>
                                <td>5</td>
                                <td>
                                    <div class="input-group">
                                        <!-- <select class="form-control" data-live-search="true">
                                            <option value=""></option>
                                        </select> -->
                                        <select class="form-control selectedPart">
                                            <option value='0'>Select Part No</option>
                                            <option value='1'>PFPN1467/8YA</option>
                                            <option value='2'>PJPNB0120ZA-B</option>
                                            <option value='3'>PJPNC0013ZA</option>
                                            <option value='4'>PJPNC0260ZA-N</option>
                                            <option value='5'>RPNX1091C-1</option>
                                        </select>
                                    </div>
                                </td>
                                <td></td>
                                <td>
                                    <div class="input-group">
                                        <select class="form-control">
                                            <option value="">D-Direct</option>
                                            <option value="">I-Indirect</option>
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <div class="input-group">
                                        <select class="form-control">
                                            <option value="">A</option>
                                            <option value="">B</option>
                                        </select>
                                    </div>
                                </td>
                                <td></td>
                                <td>
                                    <input type="text" class="form-control">
                                </td>
                                <td></td>
                                <td></td>
                                <td>
                                    <button class="btn btn-primary mx-auto d-block">
                                        Save
                                    </button>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>

            </div>

        </div>

    </div>

</body>

</html>