<?php include("../../setrelative.php") ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>jsQR</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/package/bootstrap-4.5.3/css/bootstrap.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Source+Code+Pro&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/main.css">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/bootstrap-custom.css">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/custom.css">
    <script src="../../package/scanner_js/jsQR_js/jsQR.js"></script>
    <style>
        #preview{
            width: 70%;    
            display: none;    
        }
    </style>
    <script type="text/javascript">
        // declare outside to use at scan() and stop()
        var video, camera,canvasElement,canvas, chgCamera;
        // environment - back camera; user - front camera
        var mode = "environment";
        function scan(){ 
            canvasElement = document.getElementById("preview");
            video = document.createElement("video");
            canvas = canvasElement.getContext("2d");
            chgCamera = document.getElementById('switch');
            var result = document.getElementById("result");
            var data, code;

            chgCamera.hidden = false;
            canvasElement.style.display = "block";

            // function to get the camera 
            navigator.mediaDevices.getUserMedia({ video: { facingMode: mode } }).then(function(stream) {
                video.srcObject = stream;
                video.play();
                camera = stream;
                /* the core for the camera, 
                    act like an animation and will keep request and update to the next animation
                */
                requestAnimationFrame(tick);      
            });
            
            function tick() {
                if (video.readyState === video.HAVE_ENOUGH_DATA) {
                    canvasElement.height = video.videoHeight;
                    canvasElement.width = video.videoWidth;
                    canvas.drawImage(video, 0, 0, canvasElement.width, canvasElement.height);
                    imageData = canvas.getImageData(0, 0, canvasElement.width, canvasElement.height);
                    // code store the scanning result
                    code = jsQR(imageData.data, imageData.width, imageData.height, {
                    inversionAttempts: "dontInvert",
                    });
                    
                    // when there is data, stop the camera
                    if (code) {
                        video.pause();
                        camera.getTracks()[0].stop();
                        data = code.data;
                    }
                }   
                // then, hide the video and display the result
                if(!code){
                    requestAnimationFrame(tick); 
                }else{
                    canvasElement.style.display = "none";
                    result.innerHTML = data;
                    chgCamera.hidden = true;
                }
            }
        }

        function stop(){
            video.pause();
            camera.getTracks()[0].stop();
            canvasElement.style.display = "none"; 
            chgCamera.hidden = true;
        }

        function swi_tch(){
            var mirror = document.getElementById('preview');
            stop();
            if(mode === "user"){ 
                mode = "environment"  
                mirror.style.transform = '';                    
            }else{
                mode = "user";  
                mirror.style.transform = 'rotateY(180deg)';
            }
            scan();
        }
    </script>
</head>
<body>
    <div class="container-fluid">
        <div class="jumbotron jumbotron-master" >
            <div class="page-title">
                <h3>jsQR</h3>
            </div>
            <div class="row page-section justify-content-center">
                <div class="col-md-6">
                    <div class="input-group">
                        <div class="input-group prepend">
                            <button class="btn btn-secondary btn-block" type="submit" onclick="scan()">
                            CLICK HERE TO SCAN </button> 
                            <button class="btn btn-secondary btn-block" type="submit" onclick="stop()">
                                CLICK HERE TO STOP </button>
                            <button class="btn btn-secondary btn-block" type="submit" id="switch" hidden onclick="swi_tch()">
                                SWITCH CAMERA</button>
                        </div>
                    </div>
                    <div style="text-align: center;">
                         <canvas id="preview"></canvas>
                    </div>
                   
                    <div class="input-group">
                        <div class="input-group-prepend prepend-80">
                            <span class="input-group-text">Scan Result</span>
                        </div>
                        <textarea class="form-control" id="result">Scan Result display here</textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
