<?php include("../../setrelative.php") ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Testing</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/package/bootstrap-4.5.3/css/bootstrap.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Source+Code+Pro&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/main.css">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/custom.css">


    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script> -->
    <!-- <script src="jquery-1.7.1.js" type="text/javascript"></script> -->
    <script type="text/javascript">
        $(document).ready(function() {
            $('.btnAdd').off("click");
            $('.btnAdd').on("click", function(e) {
                e.preventDefault();
                $(this).parent().parent().prev().find('select.form-control option:selected').appendTo(
                    $(this).parent().parent().next().find('select.form-control').get(0)
                ).prop('selected', false);
            });

            $('.btnAddAll').off("click");
            $('.btnAddAll').on("click", function(e) {
                e.preventDefault();
                $(this).parent().parent().prev().find('select.form-control option').appendTo(
                    $(this).parent().parent().next().find('select.form-control').get(0)
                ).prop('selected', false);
            })

            $('.btnRemove').off("click");
            $('.btnRemove').on("click", function(e) {
                e.preventDefault();
                $(this).parent().parent().next().find('select.form-control option:selected').appendTo(
                    $(this).parent().parent().prev().find('select.form-control').get(0)
                ).prop('selected', false);
            });

            $('.btnRemoveAll').off("click");
            $('.btnRemoveAll').on("click", function(e) {
                e.preventDefault();
                $(this).parent().parent().next().find('select.form-control option').appendTo(
                    $(this).parent().parent().prev().find('select.form-control').get(0)
                ).prop('selected', false);
            });
        });
    </script>
    <style>
        
        .btn-group-custom {
            width: 100%;
            position: relative;
            display: inline-flex;
            vertical-align: middle;
        }
    
        .btn-group-custom .btn {
            position: relative !important;
            flex: 1 1 auto !important;
        }

        // Bring the hover, focused, and "active" buttons to the front to overlay
        // the borders properly
        .btn-group-custom .btn:hover,
        .btn-group-custom .btn:focus,
        .btn-group-custom .btn:active,
        .btn-group-custom .btn.active {
            z-index: 1;
        }
        .btn-group-custom .btn,
        .btn-group-custom .btn-group {
            width: 100%;
        }

        .btn-group-custom .btn:not(:first-child),
        .btn-group-custom .btn-group:not(:first-child) {
            margin-top: -1;
        }

        // Reset rounded corners
        .btn-group-custom .btn:not(:last-child):not(.dropdown-toggle),
        .btn-group-custom .btn-group:not(:last-child) > .btn {
            border-radius: 0 auto;
        }

        .btn-group-custom .btn:not(:first-child),
        .btn-group-custom .btn-group:not(:first-child) > .btn {
            border-radius: 0 auto;
        }

        @media (min-width: 36em) { 
            .btn-group-custom {
                flex-direction: column !important;
                align-items: flex-start !important;
                justify-content: center !important;
            }
        }
    </style>
</head>

<body>
    <div class="container-fluid">
        <div class="jumbotron">
            <!--Title and navigation bar-->
            <div class="page-title">
                <h3>Test: Multi-selectbox</h3>
            </div>

            <div class="row page-section">
                <div class="col-md-6">
                    <!-- The coding start here -->
                    <div class="form-group">
                        <label for="formGroupExampleInput">Items to select</label>
                        <div class="row">
                            <div class="col-sm-5">
                                <select class="form-control" multiple="multiple" size=5>
                                    <option value="1">Test 1</option>
                                    <option value="2">Test 2</option>
                                    <option value="3">Test 3</option>
                                    <option value="4">Test 4</option>
                                    <option value="5">Test 5</option>
                                    <option value="6">Test 6</option>
                                </select>
                            </div>
                            <div class="col-sm-2 mx-0 px-sm-0 py-3 py-sm-0">
                                <div class="btn-group-custom btn-block btn-group-sm">
                                    <button class="btn btn-outline-primary btnAdd">
                                        <span class="d-sm-none">&darr;</span>
                                        <span class="d-none d-sm-block">&rarr;</span>
                                    </button>
                                    <button class="btn btn-outline-primary btnAddAll">
                                        <span class="d-sm-none">&#8609;</span>
                                        <span class="d-none d-sm-block">&#8608;</span>
                                    </button>
                                    <button class="btn btn-outline-primary btnRemove">
                                        <span class="d-sm-none">&uarr;</span>
                                        <span class="d-none d-sm-block">&larr;</span>
                                    </button>
                                    <button class="btn btn-outline-primary btnRemoveAll">
                                        <span class="d-sm-none">&#8607;</span>
                                        <span class="d-none d-sm-block">&#8606;</span>
                                    </button>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <select class="form-control" id="formGroupExampleInput" name="formGroupExampleInput" multiple="multiple" size=5>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- The coding end here -->
                </div>
            </div>



            <!-- <div class="col-sm-12">
                <div class="col-xs-4">
                    Items to select
                    <select id="list1" style="width:40%" multiple="multiple" class="form-control" size=5>
                        <option value="1">Test 1</option>
                        <option value="2">Test 2</option>
                        <option value="3">Test 3</option>
                        <option value="4">Test 4</option>
                        <option value="5">Test 5</option>
                    </select>
                </div>

                <div class="col-xs-2" style="width:20%">
                    <input class="btn btn-primary" type="button" id="btnAdd" value=">" style="width: 50px;" /><br />
                    <input class="btn btn-primary" type="button" id="btnAddAll" value=">>" style="width: 50px;" /><br />
                    <input class="btn btn-primary" type="button" id="btnRemove" value="<" style="width: 50px;" /><br />
                    <input class="btn btn-primary" type="button" id="btnRemoveAll" value="<<" style="width: 50px;" />
                </div>

                <div class="col-xs-4" style="width:40%">
                    Selected Items
                    <select id="list2" multiple="multiple" class="form-control" size=5>
                    </select>
                </div>
            </div> -->


        </div>
    </div>
</body>

</html>