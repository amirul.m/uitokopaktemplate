<?php include("../../setrelative.php") ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Use qr-Scanner</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/package/bootstrap-4.5.3/css/bootstrap.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Source+Code+Pro&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/main.css">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/bootstrap-custom.css">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/custom.css">
    <style>
        #preview {
            width: 70%;
            display: none;
            margin: 0 auto;
        }
    </style>
</head>
<body>
    <div class="container-fluid">
        <div class="jumbotron jumbotron-master">
            <div class="page-title">
                <h3>qr-Scanner</h3>
            </div>
            <div class="row page-section justify-content-center">
                <div class="col-md-6">
                    <div class="input-group">
                        <div class="input-group prepend">
                            <button class="btn btn-secondary btn-block" type="submit" id="scan">
                                CLICK HERE TO SCAN </button>
                            <button class="btn btn-secondary btn-block" type="submit" id="stop">
                                CLICK HERE TO STOP </button>
                            <button class="btn btn-secondary btn-block" type="submit" id="switch" hidden>
                               CANT SWITCH</button>
                        </div>
                    </div>
                    <video id="preview"></video>
                    <script type="module">
                        // TODO: cannot switch camera at phone
                        // Important setup                     
                        import QrScanner from "../../package/scanner_js/qrScanner_js/qrscanner.js";
                        QrScanner.WORKER_PATH = "../../package/scanner_js/qrScanner_js/qrscanner-worker.js";

                        const video = document.getElementById('preview');
                        const printResult = document.getElementById('result');
                        var scan = document.getElementById('scan');
                        var stop = document.getElementById('stop');
                        var chgCamera = document.getElementById('switch');
                        var mode = "environment";
                        var scanner;
                        function setResult(label, result) {
                            label.innerHTML = result;
                            stopAll();
                        }
                        function stopAll() {
                            chgCamera.hidden = true;
                            scanner.stop();
                            video.style.display = 'none';                          
                        }
                        function startAll() {
                            printResult.innerHTML = "Scanning";
                            chgCamera.hidden = false;
                            scanner.start();
                            video.style.display = 'block';
                            
                        }
                        /* function to get camera
                            can have 5 parameter
                            third is set error message
                            forth is set the scanner region
                            fifth is set the camera use
                        */
                        scanner = new QrScanner(video, result => setResult(printResult, result), undefined,
                            region => {
                                return
                                x: video.style.width;
                                y: video.style.height;
                                width: x;
                                height: y;
                                downScaleWidth: x;
                                downScaleHeight: y;
                            }, mode);

                        scan.addEventListener('click', () => startAll());
                        stop.addEventListener('click', () => stopAll());
                        document.getElementById('show-scan-region').addEventListener('change', (e) => {
                            const input = e.target;
                            const label = input.parentNode;
                            label.parentNode.insertBefore(scanner.$canvas, label.nextSibling);
                            scanner.$canvas.style.width = "70%";
                            scanner.$canvas.style.display = input.checked ? 'block' : 'none';
                        });
                    </script>
                    
                    <div class="input-group">
                        <div class="input-group-prepend prepend-80">
                            <span class="input-group-text">Scan Result</span>
                        </div>
                        <textarea class="form-control" id="result">Scan Result display here</textarea>
                    </div>
                    <br>
                    <label>
                        <input id="show-scan-region" type="checkbox">
                        Show scan region
                    </label>

                </div>
            </div>
        </div>
    </div>
</body>