<?php include("../../setrelative.php") ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Indirect Receipt</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/package/bootstrap-4.5.3/css/bootstrap.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Source+Code+Pro&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/main.css">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/bootstrap-custom.css">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/custom.css">

</head>

<body>
    <div class="container">
        <div class="jumbotron my-3">
            <div class="page-title page-title-center">
                <h3>Indirect Receipt</h3>
            </div>

            <div class="row page-section justify-content-center">
                <div class="col-md-6">
                    <div class="input-group">
                        <div class="input-group-prepend prepend-30">
                            <span class="input-group-text">Received Date</span>
                        </div>
                        <input type="date" class="form-control" placeholder="dd/mm/yyyy">
                    </div>

                    <div class="input-group">
                        <div class="input-group-prepend prepend-30">
                            <span class="input-group-text">Pallet/Trolley ID</span>
                        </div>
                        <select class="form-control">
                            <option value=""></option>
                            <option value=""> 001</option>
                            <option value=""> 002</option>
                            <option value=""> 003</option>
                        </select>
                    </div>

                    <div class="input-group">
                        <div class="input-group prepend">
                            <button class="btn btn-secondary btn-block" type="submit">SCAN QR CODE</button> 
                        </div>
                        <input type="text" class="form-control" placeholder="Click here to SCAN" readonly>
                    </div>

                    <div class="input-group">
                        <div class="input-group-prepend prepend-30">
                            <span class="input-group-text">Remark</span>
                        </div>
                        <textarea class="form-control">Remark...</textarea>
                    </div>

                    <div class="form-group">
                        <button class="btn btn-primary ml-2 float-right">Clear</button>
                    </div>
                </div>

            </div>
            <hr>
            <div class="row page-section justify-content-center">
                <div class="col-md-6 p-3">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Part No</th>
                                    <th>Qty</th>
                                    <th>LabelKey</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><input type="checkbox" name="" id=""></td>
                                    <td>001</td>
                                    <td>001</td>
                                    <td>001</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="" id=""></td>
                                    <td>abc</td>
                                    <td>002</td>
                                    <td>002</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="" id=""></td>
                                    <td>abc</td>
                                    <td>003</td>
                                    <td>003</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="form-group">
                        <button class="btn btn-primary ml-2 float-right">Submit</button>
                        <button class="btn btn-primary ml-2 float-right">Del Line</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</body>