<?php include("../../setrelative.php") ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Indirect Packing</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/package/bootstrap-4.5.3/css/bootstrap.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Source+Code+Pro&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/main.css">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/bootstrap-custom.css">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/custom.css">

</head>

<body>
    <div class="container">
        <div class="jumbotron my-3">
            <div class="page-title page-title-center">
                <h3>Indirect Packing</h3>
            </div>

            <div class="row page-section justify-content-center">
                <div class="col-md-6">
                    <div class="input-group">
                        <div class="input-group-prepend prepend-30">
                            <span class="input-group-text">Packing Date</span>
                        </div>
                        <input type="date" class="form-control" placeholder="dd/mm/yyyy">
                    </div>

                    <div class="input-group">
                        <div class="input-group prepend">
                            <button class="btn btn-secondary btn-block" type="submit">SCAN ITEM QR CODE</button> 
                        </div>
                        <input type="text" class="form-control" placeholder="Click here to scan" readonly>
                    </div>

                    <div class="input-group">
                        <div class="input-group-prepend prepend-30">
                            <span class="input-group-text">Batch No</span>
                        </div>
                        <input type="text" class="form-control" placeholder="Batch No" disabled>
                    </div>

                    <div class="input-group">
                        <div class="input-group-prepend prepend-30">
                            <span class="input-group-text">Part No</span>
                        </div>
                        <input type="text" class="form-control" placeholder="Part No" disabled>
                    </div>

                    <div class="input-group">
                        <div class="input-group-prepend prepend-30">
                            <span class="input-group-text"># of Packs Scanned</span>
                        </div>
                        <input type="text" class="form-control" placeholder="Packs Scanned" disabled>
                    </div>

                    <div class="input-group">
                        <div class="input-group-prepend prepend-30">
                            <span class="input-group-text">Received Qty</span>
                        </div>
                        <input type="text" class="form-control" placeholder="Received Qty" disabled>
                    </div>

                    <div class="input-group">
                        <div class="input-group-prepend prepend-30">
                            <span class="input-group-text">Remark</span>
                        </div>
                        <textarea class="form-control">Remark</textarea>
                    </div>

                    <div class="form-group">
                        <button class="btn btn-primary ml-2 float-right">Clear</button>
                    </div>

                    <div class="form-group">
                        <button class="btn btn-primary ml-2 float-right">Add line</button>
                    </div>                    
                </div>

            </div>
            <hr>
            <div class="row page-section justify-content-center">
                <div class="col-md-6 p-3">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Part No</th>
                                    <th>Qty</th>
                                    <th>LabelKey</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><input type="checkbox" name="" id=""></td>
                                    <td>001</td>
                                    <td>001</td>
                                    <td>001</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="" id=""></td>
                                    <td>abc</td>
                                    <td>002</td>
                                    <td>002</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="" id=""></td>
                                    <td>abc</td>
                                    <td>003</td>
                                    <td>003</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="form-group">
                        <button class="btn btn-primary ml-2 float-right">Submit</button>
                        <button class="btn btn-primary ml-2 float-right">Del Line</button>
                    </div>
                </div>
    
            </div>
        </div>
    </div>
        
</body>