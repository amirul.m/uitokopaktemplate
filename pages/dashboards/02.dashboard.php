<?php include("../../setrelative.php") ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/package/fontawesome-free-5.15.1-web/css/all.css">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/dashboard.css">
</head>
<body>
    <div class="wrapper">
        <div class="wrapper-content navigation">
            <div class="navigation-logo">
                <img class="img-fluid" src="<?php echo RelativePath; ?>/assets/image/PMTLOGOTEST.png" alt="">
            </div>
            <div class="navigation-menu menu">
                <div class="menu-option">
                    <label class="menu-option-icon" for="menu-option">
                        <i class="fas fa-bars fa-2x"></i>
                    </label>
                </div>
                <input type="checkbox" class="menu-option-checkbox" id="menu-option">
                <div class="menu-list">
                    <?php for($i = 1; $i <= 5; $i++) { ?>
                    <div class="menu-title">
                        <input type="checkbox" name="menu-main[]" class="menu-title-checkbox" id="menu-<?php echo $i; ?>">
                        <div class="menu-title-main text-center">
                            <label for="menu-<?php echo $i; ?>">Title <?php echo $i; ?></label>
                        </div>
                        <div class="menu-title-submenu submenu">
                            <ul class="submenu-list">
                                <li class="submenu-item">
                                    <div class="submenu-item-main">
                                        <label>Menu asdasdasdasd <?php echo $i; ?>1</label>
                                    </div>
                                </li>
                                <li class="submenu-item submenu-title">
                                    <input type="checkbox" name="submenu-sub-<?php echo $i; ?>[]" class="submenu-title-checkbox" id="submenu-<?php echo $i; ?>2">
                                    <div class="submenu-title-main">
                                        <label for="submenu-<?php echo $i; ?>2">Sub Title <?php echo $i; ?>2 <i class="menu-arrow fas fa-chevron-right"></i></label>
                                    </div>
                                    <div class="submenu-title-submenu submenu-sub">
                                        <ul class="submenu-sub-list">
                                            <li class="submenu-sub-item">Menu <?php echo $i; ?>21</li>
                                            <li class="submenu-sub-item">Menu <?php echo $i; ?>22</li>
                                            <li class="submenu-sub-item">Menu <?php echo $i; ?>23</li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="submenu-item">
                                    <div class="submenu-item-main">
                                        <label>Menu asdasdasdasd <?php echo $i; ?>3</label>
                                    </div>
                                </li>
                                <li class="submenu-item submenu-title">
                                    <input type="checkbox" name="submenu-sub-<?php echo $i; ?>[]" class="submenu-title-checkbox" id="submenu-<?php echo $i; ?>4">
                                    <div class="submenu-title-main">
                                        <label for="submenu-<?php echo $i; ?>4">asda dsad as a sd <?php echo $i; ?>4 <i class="menu-arrow fas fa-chevron-right"></i></label>
                                    </div>
                                    <div class="submenu-title-submenu submenu-sub">
                                        <ul class="submenu-sub-list">
                                            <li class="submenu-sub-item">
                                                <div class="submenu-sub-item-main">
                                                    Menu <?php echo $i; ?>41
                                                </div>
                                            </li>
                                            <li class="submenu-sub-item">
                                                <div class="submenu-sub-item-main">
                                                    Menu <?php echo $i; ?>42
                                                </div>
                                            </li>
                                            <li class="submenu-sub-item submenu-title">
                                                <input type="checkbox" name="submenu-sub-<?php echo $i; ?>41[]" class="submenu-title-checkbox" id="submenu-<?php echo $i; ?>41">
                                                <div class="submenu-title-main">
                                                    <label for="submenu-<?php echo $i; ?>41">Sub Title <?php echo $i; ?>41 <i class="menu-arrow fas fa-chevron-right"></i></label>
                                                </div>
                                                <div class="submenu-title-submenu submenu-sub">
                                                    <ul class="submenu-sub-list">
                                                        <li class="submenu-sub-item">
                                                            <div class="submenu-sub-item-main">
                                                                Menu <?php echo $i; ?>411
                                                            </div>
                                                        </li>
                                                        <li class="submenu-sub-item">
                                                            <div class="submenu-sub-item-main">
                                                                Menu <?php echo $i; ?>412
                                                            </div>
                                                        </li>
                                                        <li class="submenu-sub-item">
                                                            <div class="submenu-sub-item-main">
                                                                Menu <?php echo $i; ?>413
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <div class="navigation-setting setting">
                <label class="setting-icon" for="checkbox-setting">
                    <i class="fas fa-user fa-2x"></i>
                </label>
                <input type="checkbox" class="setting-switcher" id="checkbox-setting">
                <div class="setting-body">
                    <ul class="setting-body-menu">
                        <li class="setting-body-button">
                            <label class="setting-body-button-icon" for="checkbox-setting">
                                <i class="fas fa-times fa-lg"></i>
                            </label>
                        </li>
                        <li class="setting-body-list">
                            <a href="#">Account</a>
                        </li>
                        <li class="setting-body-list">
                            <a href="#">Setting</a>
                        </li>
                        <li class="setting-body-list">
                            <a href="#">Help</a>
                        </li>
                        <li class="setting-body-list">
                            <a href="#">Logout</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wrapper-content main">main</div>
        <div class="wrapper-content footer">
            <div class="footer-copyright">
                <p>Copyright © 2002-2021 Xeersoft Sdn Bhd</p>
                <p>All rights reserved.</p>
            </div>
        </div>
    </div>

    <script src="<?php echo RelativePath; ?>/package/jquery-3.5.1/jquery-3.5.1.min.js"></script>
    <script src="<?php echo RelativePath; ?>/assets/js/dashboard.js"></script>
</body>
</html>