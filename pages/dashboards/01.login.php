<?php include("../../setrelative.php") ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login Page</title>
    
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/package/bootstrap-4.5.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/package/fontawesome-free-5.15.1-web/css/all.css">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/custom.css">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/login.css">
</head>
<body>
    <div class="wrapper">
        <div class="wrapper-content form-wrapper">
            <form action="" class="form">
                <div class="form-header">
                    <img class="img-fluid" src="<?php echo RelativePath; ?>/assets/image/PMTLOGOTEST.png" alt="">
                    <h3>Welcome back</h3>
                </div>
                <div class="form-body container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="input-group">
                                <div class="input-group-prepend prepend-30">
                                    <span class="input-group-text">Username</span>
                                </div>
                                <input type="text" class="form-control" placeholder="Input">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="input-group">
                                <div class="input-group-prepend prepend-30">
                                    <span class="input-group-text">Password</span>
                                </div>
                                <input type="text" class="form-control" placeholder="Input">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <button class="btn btn-primary ml-2 float-right"> 
                                    Login
                                </button>
                            </div> 
                        </div>
                    </div>
                </div>
                <div class="form-footer">
                    <div class="form-footer-copyright">
                        <p>Copyright © 2002-2021 Xeersoft Sdn Bhd</p>
                        <p>All rights reserved.</p>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <script src="<?php echo RelativePath; ?>/package/jquery-3.5.1/jquery-3.5.1.min.js"></script>
    <script src="<?php echo RelativePath; ?>/assets/js/login.js"></script>
</body>
</html>