<?php include("../../setrelative.php") ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Generate Batch</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="<?php echo RelativePath; ?>/package/bootstrap-4.5.3/css/bootstrap.min.css">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Source+Code+Pro&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/main.css">
        <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/custom.css">
        <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/bootstrap-custom.css">

        <!-- Icon library- Font Awesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    </head>

    <body>
        <div class="container-fluid">
            <div class="jumbotron">
                <!--Title and navigation bar-->
                <div class="page-title">
                    <h3>Generate Batch</h3>
                </div>

                <div class="row page-section">
                    <div class="col-lg-4 order-2 order-lg-1">
                        <div class="row">
                            <div class="col-12">
                                <div class="input-group">
                                    <div class="input-group-prepend prepend-30">
                                        <span class="input-group-text">Generate Date</span>
                                    </div>
                                    <input type="date" name="" id="" class="form-control">
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="input-group">
                                    <div class="input-group-prepend prepend-30">
                                        <span class="input-group-text">Shift</span>
                                    </div>
                                    <select class="form-control" id="" name="">
                                        <option value="">A</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="input-group">
                                    <div class="input-group-prepend prepend-30">
                                        <span class="input-group-text">Machine</span>
                                    </div>
                                    <select class="form-control" id="" name="">
                                        <option value="">A</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="input-group">
                                    <div class="input-group-prepend prepend-30">
                                        <span class="input-group-text">Serial No.</span>
                                    </div>
                                    <select class="form-control" id="" name="">
                                        <option value="">A</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12">
                                <h4>Batch Number to be Generate</h4>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="" id="">
                                </div>
                                <div class="input-group">
                                    <div class="input-group-prepend control-50">
                                        <button class="btn btn-primary btn-block">Reset</button>
                                    </div>
                                    <div class="input-group-append control-50">
                                        <button class="btn btn-primary btn-block">Generate</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="input-group">
                                    <div class="input-group-prepend prepend-30">
                                        <span class="input-group-text">Show Batch No</span>
                                    </div>
                                    <select class="form-control" id="" name="">
                                        <option value="">A</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12">
                                <p>Please SELECT Batch No</p>
                                <div class="input-group">
                                    <select class="form-control" id="" name="">
                                        <option value="">A</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="input-group">
                                    <div class="input-group-prepend prepend-30">
                                        <span class="input-group-text">Wordings at Label</span>
                                    </div>
                                    <select class="form-control" id="" name="">
                                        <option value="">A</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="input-group">
                                    <div class="input-group-prepend prepend-30">
                                        <span class="input-group-text">Label Template</span>
                                    </div>
                                    <select class="form-control" id="" name="">
                                        <option value="">A</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12">
                                <button class="btn btn-primary float-right">Print</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 order-1 order-lg-2">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="input-group">
                                    <div class="input-group-prepend prepend-30">
                                        <span class="input-group-text">Planning Date</span>
                                    </div>
                                    <input type="date" name="" id="" class="form-control">
                                </div>
                            </div>
                            <div class="col-12 mb-3 table-responsive">
                                <table class="table table-bordered text-nowrap">
                                    <thead>
                                        <tr>
                                            <th style="min-width: 15%;">&nbsp;</th>
                                            <th style="min-width: 15%;">Planned Date <!-- I think remove this column --></th>
                                            <th style="min-width: 15%;">Machine</th>
                                            <th style="min-width: 5%;">Shift</th>
                                            <th style="min-width: 10%;">Model (Part)</th>
                                            <th style="min-width: 10%;">Description</th>
                                            <th style="min-width: 10%;">Quantity</th>
                                            <th style="min-width: 10%;">Per Pack</th>
                                            <th style="min-width: 10%;">Labels</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><input type="checkbox" name="" id=""></td>
                                            <td><input type="text" class="form-control" name="" id="" disabled></td>
                                            <td><input type="text" class="form-control" name="" id="" disabled></td>
                                            <td><input type="text" class="form-control" name="" id="" disabled></td>
                                            <td><input type="text" class="form-control" name="" id="" disabled></td>
                                            <td><input type="text" class="form-control" name="" id="" disabled></td>
                                            <td><input type="text" class="form-control" name="" id="" disabled></td>
                                            <td><input type="text" class="form-control" name="" id="" disabled></td>
                                            <td><input type="text" class="form-control" name="" id="" disabled></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-12">
                                <button class="btn btn-primary mb-3 float-right">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>