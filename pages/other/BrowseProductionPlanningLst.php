<?php include("../../setrelative.php") ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Browse Production Planning Schedule</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/package/bootstrap-4.5.3/css/bootstrap.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Source+Code+Pro&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/main.css">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/custom.css">

</head>

<body>
    <div class="container-fluid">
        <div class="jumbotron">
            <!--Title and navigation bar-->
            <div class="page-title">
                <h3>Browse Production Planning Schedule</h3>
            </div>

            <div class="row page-section">
                <div class="col-12">
                    <div class="form-group mb-0">
                        <button class="btn btn-primary">
                            GOTO Planning
                        </button>
                    </div>
                </div>
            </div>


            <!-- Listing-->
            <div class="row page-section">
                <div class="col-12">

                    <div class="table-wrap">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Plan Date</th>
                                    <th>M1</th>
                                    <th>M2</th>
                                    <th>M3</th>
                                    <th>M4</th>
                                    <th>M5</th>
                                    <th>M6</th>
                                    <th>M7</th>
                                    <th>M8</th>
                                    <th>M9</th>
                                    <th>M10</th>
                                    <th>M11</th>
                                    <th>M12</th>
                                    <th>M13</th>
                                    <th>M14</th>
                                    <th>M15</th>
                                    <th>M16</th>
                                    <th>M17</th>
                                    <th>M18</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Row 1 with column 1</td>
                                    <td>Row 1 with column 2</td>
                                    <td>Row 1 with column 3</td>
                                    <td>Row 1 with column 4</td>
                                    <td>Row 1 with column 5</td>
                                    <td>Row 1 with column 6</td>
                                    <td>Row 1 with column 7</td>
                                    <td>Row 1 with column 8</td>
                                    <td>Row 1 with column 9</td>
                                    <td>Row 1 with column 10</td>
                                    <td>Row 1 with column 11</td>
                                    <td>Row 1 with column 12</td>
                                    <td>Row 1 with column 13</td>
                                    <td>Row 1 with column 14</td>
                                    <td>Row 1 with column 15</td>
                                    <td>Row 1 with column 16</td>
                                    <td>Row 1 with column 17</td>
                                    <td>Row 1 with column 18</td>
                                    <td>Row 1 with column 19</td>
                                </tr>
                                <tr>
                                    <td>Row 2 with column 1</td>
                                    <td>Row 2 with column 2</td>
                                    <td>Row 2 with column 3</td>
                                    <td>Row 2 with column 4</td>
                                    <td>Row 2 with column 5</td>
                                    <td>Row 2 with column 6</td>
                                    <td>Row 2 with column 7</td>
                                    <td>Row 2 with column 8</td>
                                    <td>Row 2 with column 9</td>
                                    <td>Row 2 with column 10</td>
                                    <td>Row 2 with column 11</td>
                                    <td>Row 2 with column 12</td>
                                    <td>Row 2 with column 13</td>
                                    <td>Row 2 with column 14</td>
                                    <td>Row 2 with column 15</td>
                                    <td>Row 2 with column 16</td>
                                    <td>Row 2 with column 17</td>
                                    <td>Row 2 with column 18</td>
                                    <td>Row 2 with column 19</td>
                                </tr>
                                <tr>
                                    <td>Row 3 with column 1</td>
                                    <td>Row 3 with column 2</td>
                                    <td>Row 3 with column 3</td>
                                    <td>Row 3 with column 4</td>
                                    <td>Row 3 with column 5</td>
                                    <td>Row 3 with column 6</td>
                                    <td>Row 3 with column 7</td>
                                    <td>Row 3 with column 8</td>
                                    <td>Row 3 with column 9</td>
                                    <td>Row 3 with column 10</td>
                                    <td>Row 3 with column 11</td>
                                    <td>Row 3 with column 12</td>
                                    <td>Row 3 with column 13</td>
                                    <td>Row 3 with column 14</td>
                                    <td>Row 3 with column 15</td>
                                    <td>Row 3 with column 16</td>
                                    <td>Row 3 with column 17</td>
                                    <td>Row 3 with column 18</td>
                                    <td>Row 3 with column 19</td>
                                </tr>
                                <tr>
                                    <td>Row 4 with column 1</td>
                                    <td>Row 4 with column 2</td>
                                    <td>Row 4 with column 3</td>
                                    <td>Row 4 with column 4</td>
                                    <td>Row 4 with column 5</td>
                                    <td>Row 4 with column 6</td>
                                    <td>Row 4 with column 7</td>
                                    <td>Row 4 with column 8</td>
                                    <td>Row 4 with column 9</td>
                                    <td>Row 4 with column 10</td>
                                    <td>Row 4 with column 11</td>
                                    <td>Row 4 with column 12</td>
                                    <td>Row 4 with column 13</td>
                                    <td>Row 4 with column 14</td>
                                    <td>Row 4 with column 15</td>
                                    <td>Row 4 with column 16</td>
                                    <td>Row 4 with column 17</td>
                                    <td>Row 4 with column 18</td>
                                    <td>Row 4 with column 19</td>
                                </tr>
                                <tr>
                                    <td>Row 5 with column 1</td>
                                    <td>Row 5 with column 2</td>
                                    <td>Row 5 with column 3</td>
                                    <td>Row 5 with column 4</td>
                                    <td>Row 5 with column 5</td>
                                    <td>Row 5 with column 6</td>
                                    <td>Row 5 with column 7</td>
                                    <td>Row 5 with column 8</td>
                                    <td>Row 5 with column 9</td>
                                    <td>Row 5 with column 10</td>
                                    <td>Row 5 with column 11</td>
                                    <td>Row 5 with column 12</td>
                                    <td>Row 5 with column 13</td>
                                    <td>Row 5 with column 14</td>
                                    <td>Row 5 with column 15</td>
                                    <td>Row 5 with column 16</td>
                                    <td>Row 5 with column 17</td>
                                    <td>Row 5 with column 18</td>
                                    <td>Row 5 with column 19</td>
                                </tr>
                                <tr>
                                    <td>Row 6 with column 1</td>
                                    <td>Row 6 with column 2</td>
                                    <td>Row 6 with column 3</td>
                                    <td>Row 6 with column 4</td>
                                    <td>Row 6 with column 5</td>
                                    <td>Row 6 with column 6</td>
                                    <td>Row 6 with column 7</td>
                                    <td>Row 6 with column 8</td>
                                    <td>Row 6 with column 9</td>
                                    <td>Row 6 with column 10</td>
                                    <td>Row 6 with column 11</td>
                                    <td>Row 6 with column 12</td>
                                    <td>Row 6 with column 13</td>
                                    <td>Row 6 with column 14</td>
                                    <td>Row 6 with column 15</td>
                                    <td>Row 6 with column 16</td>
                                    <td>Row 6 with column 17</td>
                                    <td>Row 6 with column 18</td>
                                    <td>Row 6 with column 19</td>
                                </tr>
                                <tr>
                                    <td>Row 7 with column 1</td>
                                    <td>Row 7 with column 2</td>
                                    <td>Row 7 with column 3</td>
                                    <td>Row 7 with column 4</td>
                                    <td>Row 7 with column 5</td>
                                    <td>Row 7 with column 6</td>
                                    <td>Row 7 with column 7</td>
                                    <td>Row 7 with column 8</td>
                                    <td>Row 7 with column 9</td>
                                    <td>Row 7 with column 10</td>
                                    <td>Row 7 with column 11</td>
                                    <td>Row 7 with column 12</td>
                                    <td>Row 7 with column 13</td>
                                    <td>Row 7 with column 14</td>
                                    <td>Row 7 with column 15</td>
                                    <td>Row 7 with column 16</td>
                                    <td>Row 7 with column 17</td>
                                    <td>Row 7 with column 18</td>
                                    <td>Row 7 with column 19</td>
                                </tr>
                                <tr>
                                    <td>Row 8 with column 1</td>
                                    <td>Row 8 with column 2</td>
                                    <td>Row 8 with column 3</td>
                                    <td>Row 8 with column 4</td>
                                    <td>Row 8 with column 5</td>
                                    <td>Row 8 with column 6</td>
                                    <td>Row 8 with column 7</td>
                                    <td>Row 8 with column 8</td>
                                    <td>Row 8 with column 9</td>
                                    <td>Row 8 with column 10</td>
                                    <td>Row 8 with column 11</td>
                                    <td>Row 8 with column 12</td>
                                    <td>Row 8 with column 13</td>
                                    <td>Row 8 with column 14</td>
                                    <td>Row 8 with column 15</td>
                                    <td>Row 8 with column 16</td>
                                    <td>Row 8 with column 17</td>
                                    <td>Row 8 with column 18</td>
                                    <td>Row 8 with column 19</td>
                                </tr>
                                <tr>
                                    <td>Row 9 with column 1</td>
                                    <td>Row 9 with column 2</td>
                                    <td>Row 9 with column 3</td>
                                    <td>Row 9 with column 4</td>
                                    <td>Row 9 with column 5</td>
                                    <td>Row 9 with column 6</td>
                                    <td>Row 9 with column 7</td>
                                    <td>Row 9 with column 8</td>
                                    <td>Row 9 with column 9</td>
                                    <td>Row 9 with column 10</td>
                                    <td>Row 9 with column 11</td>
                                    <td>Row 9 with column 12</td>
                                    <td>Row 9 with column 13</td>
                                    <td>Row 9 with column 14</td>
                                    <td>Row 9 with column 15</td>
                                    <td>Row 9 with column 16</td>
                                    <td>Row 9 with column 17</td>
                                    <td>Row 9 with column 18</td>
                                    <td>Row 9 with column 19</td>
                                </tr>


                            </tbody>
                        </table>
                    </div>

                </div>
            </div>


        </div>


    </div>

</body>

</html>