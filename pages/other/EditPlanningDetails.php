<?php include("../../setrelative.php") ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Edit Planning Details</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/package/bootstrap-4.5.3/css/bootstrap.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Source+Code+Pro&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/main.css">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/custom.css">

    <!-- Icon library- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

</head>

<body>
    <div class="container-fluid">
        <div class="jumbotron">
            <!--Title and navigation bar-->
            <div class="page-title">
                <h3>Edit Planning Details, Machine 1</h3>
            </div>

            <div class="row page-section">
                <div class="col-12">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th style="width: 5%">Model</th>
                                <th style="width: 15%">Part No</th>
                                <th style="width: 15%">Description</th>
                                <th style="width: 10%">Type</th>
                                <th style="width: 5%">Shift</th>
                                <th style="width: 10%">Per Pack</th>
                                <th style="width: 10%">Total Cycle</th>
                                <th style="width: 5%">Cavity</th>
                                <th style="width: 10%">Actual Qty</th>
                                <th style="width: 5%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>
                                    <div class="input-group">
                                        <select class="form-control" data-live-search="true">
                                            <option value=""></option>
                                        </select>
                                        <span class="input-group-append">
                                            <button class="btn btn-outline customclr-btn" type="button">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                </td>
                                <td></td>
                                <td>
                                    <div class="input-group">
                                        <select class="form-control">
                                            <option value="">D-Direct</option>
                                            <option value="">I-Indirect</option>
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <div class="input-group">
                                        <select class="form-control">
                                            <option value="">A</option>
                                            <option value="">B</option>
                                        </select>
                                    </div>
                                </td>
                                <td></td>
                                <td>
                                    <input type="text" class="form-control">
                                </td>
                                <td></td>
                                <td></td>
                                <td>
                                    <button class="btn btn-primary mx-auto d-block">
                                        Save
                                    </button>
                                </td>
                            </tr>


                            <tr>
                                <td>2</td>
                                <td>
                                    <div class="input-group">
                                        <select class="form-control" data-live-search="true">
                                            <option value=""></option>
                                        </select>
                                        <span class="input-group-append">
                                            <button class="btn btn-outline customclr-btn" type="button">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                </td>
                                <td></td>
                                <td>
                                    <div class="input-group">
                                        <select class="form-control">
                                            <option value="">D-Direct</option>
                                            <option value="">I-Indirect</option>
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <div class="input-group">
                                        <select class="form-control">
                                            <option value="">A</option>
                                            <option value="">B</option>
                                        </select>
                                    </div>
                                </td>
                                <td></td>
                                <td>
                                    <input type="text" class="form-control">
                                </td>
                                <td></td>
                                <td></td>
                                <td>
                                    <button class="btn btn-primary mx-auto d-block">
                                        Save
                                    </button>
                                </td>
                            </tr>


                            <tr>
                                <td>3</td>
                                <td>
                                    <div class="input-group">
                                        <select class="form-control" data-live-search="true">
                                            <option value=""></option>
                                        </select>
                                        <span class="input-group-append">
                                            <button class="btn btn-outline customclr-btn" type="button">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                </td>
                                <td></td>
                                <td>
                                    <div class="input-group">
                                        <select class="form-control">
                                            <option value="">D-Direct</option>
                                            <option value="">I-Indirect</option>
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <div class="input-group">
                                        <select class="form-control">
                                            <option value="">A</option>
                                            <option value="">B</option>
                                        </select>
                                    </div>
                                </td>
                                <td></td>
                                <td>
                                    <input type="text" class="form-control">
                                </td>
                                <td></td>
                                <td></td>
                                <td>
                                    <button class="btn btn-primary mx-auto d-block">
                                        Save
                                    </button>
                                </td>
                            </tr>

                            <tr>
                                <td>4</td>
                                <td>
                                    <div class="input-group">
                                        <select class="form-control" data-live-search="true">
                                            <option value=""></option>
                                        </select>
                                        <span class="input-group-append">
                                            <button class="btn btn-outline customclr-btn" type="button">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                </td>
                                <td></td>
                                <td>
                                    <div class="input-group">
                                        <select class="form-control">
                                            <option value="">D-Direct</option>
                                            <option value="">I-Indirect</option>
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <div class="input-group">
                                        <select class="form-control">
                                            <option value="">A</option>
                                            <option value="">B</option>
                                        </select>
                                    </div>
                                </td>
                                <td></td>
                                <td>
                                    <input type="text" class="form-control">
                                </td>
                                <td></td>
                                <td></td>
                                <td>
                                    <button class="btn btn-primary mx-auto d-block">
                                        Save
                                    </button>
                                </td>
                            </tr>

                            <tr>
                                <td>5</td>
                                <td>
                                    <div class="input-group">
                                        <select class="form-control" data-live-search="true">
                                            <option value=""></option>
                                        </select>
                                        <span class="input-group-append">
                                            <button class="btn btn-outline customclr-btn" type="button">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                </td>
                                <td></td>
                                <td>
                                    <div class="input-group">
                                        <select class="form-control">
                                            <option value="">D-Direct</option>
                                            <option value="">I-Indirect</option>
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <div class="input-group">
                                        <select class="form-control">
                                            <option value="">A</option>
                                            <option value="">B</option>
                                        </select>
                                    </div>
                                </td>
                                <td></td>
                                <td>
                                    <input type="text" class="form-control">
                                </td>
                                <td></td>
                                <td></td>
                                <td>
                                    <button class="btn btn-primary mx-auto d-block">
                                        Save
                                    </button>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>

            </div>

        </div>

    </div>

</body>

</html>