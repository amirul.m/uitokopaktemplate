<?php include("../../setrelative.php") ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>New Production Planning</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/package/bootstrap-4.5.3/css/bootstrap.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Source+Code+Pro&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/main.css">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/custom.css">

</head>

<body>
    <div class="container-fluid">
        <div class="jumbotron">
            <!--Title and navigation bar-->
            <div class="page-title">
                <h3>New Production Planning</h3>
            </div>

            <div class="row page-section">
                <div class="col-md-4">
                    <div class="input-group">
                        <div class="input-group-prepend prepend-30">
                            <span class="input-group-text">Planning Date</span>
                        </div>
                        <input type="date" class="form-control" placeholder="Input">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group">
                        <div class="input-group-prepend prepend-30">
                            <span class="input-group-text">Status</span>
                        </div>
                        <select class="form-control">
                            <option value="Draft">Draft</option>
                            <option value="Finalized">Finalized</option>
                        </select>
                    </div>
                </div>
            </div>


            <!-- Table: 5 columns per row -->
            <!-- <div class="row page-section">
                <div class="col-12">
                    <table class="table table-bordered blackborder">
                        <thead>
                            <tr>
                                <th class="text-white bg-dark">Model</th>
                                <th class="bg-success text-white">(M-1)</th>
                                <th class="bg-success text-white">(M-2)</th>
                                <th class="bg-secondary text-white">(M-3)</th>
                                <th class="bg-success text-white">(M-4)</th>
                                <th class="bg-success text-white">(M-5)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>


            <div class="row page-section">
                <div class="col-12">
                    <table class="table table-bordered blackborder">
                        <thead>
                            <tr>
                                <th class="text-white bg-dark">Model</th>
                                <th class="bg-success text-white">(M-6)</th>
                                <th class="bg-success text-white">(M-7)</th>
                                <th class="bg-success text-white">(M-8)</th>
                                <th class="bg-success text-white">(M-9)</th>
                                <th>(M-10)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>


            <div class="row page-section">
                <div class="col-12">
                    <table class="table table-bordered blackborder">
                        <thead>
                            <tr>
                                <th class="text-white bg-dark">Model</th>
                                <th>(M-11)</th>
                                <th>(M-12)</th>
                                <th>(M-14)</th>
                                <th>(M-14)</th>
                                <th>(M-15)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>


            <div class="row page-section">
                <div class="col-12">
                    <table class="table table-bordered blackborder">
                        <thead>
                            <tr>
                                <th class="text-white bg-dark">Model</th>
                                <th>(M-16)</th>
                                <th>(M-17)</th>
                                <th>(M-18)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div> -->


            <!-- Table: 10 columns per row -->
            <div class="row page-section">
                <div class="col-12">
                    <table class="table table-bordered blackborder">
                        <thead>
                            <tr>
                                <th class="text-white bg-dark">Model</th>
                                <th class="bg-success text-white">(M-1)</th>
                                <th class="bg-success text-white">(M-2)</th>
                                <th class="bg-secondary text-white">(M-3)</th>
                                <th class="bg-success text-white">(M-4)</th>
                                <th class="bg-success text-white">(M-5)</th>
                                <th class="bg-success text-white">(M-6)</th>
                                <th class="bg-success text-white">(M-7)</th>
                                <th class="bg-success text-white">(M-8)</th>
                                <th class="bg-success text-white">(M-9)</th>
                                <th>(M-10)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>


            <div class="row page-section">
                <div class="col-12">
                    <table class="table table-bordered blackborder">
                        <thead>
                            <tr>
                                <th class="text-white bg-dark">Model</th>
                                <th>(M-11)</th>
                                <th>(M-12)</th>
                                <th>(M-13)</th>
                                <th>(M-14)</th>
                                <th>(M-15)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>

                            </tr>
                            <tr>
                                <td>4</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>

                            </tr>
                            <tr>
                                <td>5</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>

                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row page-section">
                <div class="col-md-6">
                    <div class="input-group">
                        <div class="input-group-prepend prepend-20">
                            <span class="input-group-text">Remark</span>
                        </div>
                        <textarea class="form-control" row="10"></textarea>
                    </div>
                </div>

                <div class="col-12">
                    <div class="form-group mb-0">
                        <button class="btn btn-primary ml-2 float-right">
                            Save
                        </button>
                        <button class="btn btn-primary ml-2 float-right">
                            Close
                        </button>
                    </div>
                </div>
            </div>


        </div>
    </div>
</body>

</html>