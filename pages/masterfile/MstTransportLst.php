<?php include("../../setrelative.php") ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>TransportLst</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/package/bootstrap-4.5.3/css/bootstrap.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Source+Code+Pro&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/main.css">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/bootstrap-custom.css">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/custom.css">

</head>

<body>
    <div class="container-fluid">
        <div class="jumbotron jumbotron-master">
            <!--Title and navigation bar-->
            <div class="page-title">
                <h3>Transport Master</h3>
            </div>

            <!--Msttransport input field-->
            <div class="row page-section">
                <div class="col-md-4">
                    <div class="input-group">
                        <div class="input-group-prepend prepend-30">
                            <span class="input-group-text">Transport Name</span>
                        </div>
                        <input type="text" class="form-control" placeholder="Transport Name">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group">
                        <div class="input-group-prepend prepend-30">
                            <span class="input-group-text">Active</span>
                        </div>
                        <select class="form-control">
                            <option value="">All</option>
                            <option value="">Active</option>
                            <option value="">Inactive</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-12">
                <div class="form-group mb-0">
                    <button class="btn btn-primary mr-2">
                        New
                    </button>
                    <button class="btn btn-primary ml-2 float-right">
                        Search
                    </button>
                </div>
            </div>

            <!--Mstdriver Listing-->
            <div class="row page-section">
                <div class="col-12">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Active</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Row 1 with column 1</td>
                                <td>Row 1 with column 2</td>
                            </tr>
                            <tr>
                                <td>Row 2 with column 1</td>
                                <td>Row 2 with column 2</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</body>

</html>