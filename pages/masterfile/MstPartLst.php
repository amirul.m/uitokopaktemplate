<?php include("../../setrelative.php") ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>PartLst</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/package/bootstrap-4.5.3/css/bootstrap.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Source+Code+Pro&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/main.css">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/bootstrap-custom.css">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/custom.css">

</head>

<body>
    <div class="container-fluid">
        <div class="jumbotron jumbotron-master">
            <!--Title and navigation bar-->
            <div class="page-title">
                <h3>Part Master</h3>
            </div>

            <!--Mstpart input field-->
            <div class="row page-section">
                <div class="col-md-4">
                    <div class="input-group">
                        <div class="input-group-prepend prepend-30">
                            <span class="input-group-text">Part Code</span>
                        </div>
                        <input type="text" class="form-control" placeholder="Part Code">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group">
                        <div class="input-group-prepend prepend-30">
                            <span class="input-group-text">Part Name</span>
                        </div>
                        <input type="text" class="form-control" placeholder="Part Name">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group" style="display: none">
                        <div class="input-group-prepend prepend-30">
                            <span class="input-group-text">Item Type</span>
                        </div>
                        <select class="form-control">
                            <option value="">Select option</option>
                            <option value="">Finished Goods</option>
                            <option value="">Components</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group">
                        <div class="input-group-prepend prepend-30">
                            <span class="input-group-text">Customer</span>
                        </div>
                        <select class="form-control">
                            <option value="">Select option</option>
                            <option value="">Option 1</option>
                            <option value="">Option 2</option>
                            <option value="">Option 3</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group">
                        <div class="input-group-prepend prepend-30">
                            <span class="input-group-text">PN No</span>
                        </div>
                        <input type="text" class="form-control" placeholder="PN No">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group">
                        <div class="input-group-prepend prepend-30">
                            <span class="input-group-text">Part</span>
                        </div>
                        <input type="text" class="form-control" placeholder="Part">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group">
                        <div class="input-group-prepend prepend-30">
                            <span class="input-group-text">Model</span>
                        </div>
                        <input type="text" class="form-control" placeholder="Model">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group">
                        <div class="input-group-prepend prepend-30">
                            <span class="input-group-text">UOM</span>
                        </div>
                        <input type="text" class="form-control" placeholder="UOM">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group">
                        <div class="input-group-prepend prepend-30">
                            <span class="input-group-text">Active</span>
                        </div>
                        <select class="form-control">
                            <option value="">All</option>
                            <option value="">Active</option>
                            <option value="">Inactive</option>
                        </select>
                    </div>
                </div>

            </div>

            <div class="col-12">
                <div class="form-group mb-0">
                    <button class="btn btn-primary mr-2">
                        New
                    </button>
                    <button class="btn btn-primary ml-2 float-right">
                        Search
                    </button>
                </div>
            </div>

            <!--Mstpart Listing-->
            <div class="row page-section">
                <div class="col-12">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Part Code</th>
                                <th>Part Name</th>
                                <th>Customer</th>
                                <th>PN No</th>
                                <th>Part</th>
                                <th>Model</th>
                                <th>Cavity</th>
                                <th>Pcs Per Pack</th>
                                <th>Pcs Per Pallet</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Row 1 with column 1</td>
                                <td>Row 1 with column 2</td>
                                <td>Row 1 with column 3</td>
                                <td>Row 1 with column 4</td>
                                <td>Row 1 with column 5</td>
                                <td>Row 1 with column 6</td>
                                <td>Row 1 with column 7</td>
                                <td>Row 1 with column 8</td>
                                <td>Row 1 with column 9</td>
                            </tr>
                            <tr>
                                <td>Row 2 with column 1</td>
                                <td>Row 2 with column 2</td>
                                <td>Row 2 with column 3</td>
                                <td>Row 2 with column 4</td>
                                <td>Row 2 with column 5</td>
                                <td>Row 2 with column 6</td>
                                <td>Row 2 with column 7</td>
                                <td>Row 2 with column 8</td>
                                <td>Row 2 with column 9</td>
                            </tr>
                            <tr>
                                <td>Row 3 with column 1</td>
                                <td>Row 3 with column 2</td>
                                <td>Row 3 with column 3</td>
                                <td>Row 3 with column 4</td>
                                <td>Row 3 with column 5</td>
                                <td>Row 3 with column 6</td>
                                <td>Row 3 with column 7</td>
                                <td>Row 3 with column 8</td>
                                <td>Row 3 with column 9</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</body>

</html>