<?php include("../../setrelative.php") ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>TruckMnt</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/package/bootstrap-4.5.3/css/bootstrap.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Source+Code+Pro&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/main.css">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/bootstrap-custom.css">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/custom.css">

</head>

<body>
    <div class="container-fluid">
        <div class="jumbotron jumbotron-master">
            <!--Title and navigation bar-->
            <div class="page-title">
                <h3>Truck Master</h3>
            </div>
            <div class="row page-section">
                <div class="col-md-6">
                    <div class="input-group">
                        <div class="input-group-prepend prepend-20">
                            <span class="input-group-text">Truck Plate</span>
                        </div>
                        <input type="text" class="form-control" placeholder="Truck Plate">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input-group">
                        <div class="input-group-prepend prepend-20">
                            <span class="input-group-text">Transporter</span>
                        </div>
                        <input type="text" class="form-control" placeholder="Transporter">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input-group">
                        <div class="input-group-prepend prepend-20">
                            <span class="input-group-text">Active</span>
                        </div>

                        <div class="form-control control-height-auto">
                            <div class="form-check form-check-inline">
                                <input type="checkbox" class="form-check-input" id="checkbox1">
                                <label class="form-check-label" for="checkbox1"></label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12">
                    <div class="form-group mb-0">
                        <button class="btn btn-primary ml-2 float-right">
                            Cancel
                        </button>
                        <button class="btn btn-primary ml-2 float-right">
                            Delete
                        </button>
                        <button class="btn btn-primary ml-2 float-right">
                            Submit
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>