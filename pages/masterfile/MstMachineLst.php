<?php include("../../setrelative.php") ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>MachineLst</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/package/bootstrap-4.5.3/css/bootstrap.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Source+Code+Pro&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/main.css">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/bootstrap-custom.css">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/custom.css">

</head>

<body>
    <div class="container-fluid">
        <div class="jumbotron jumbotron-master">
            <!--Title and navigation bar-->
            <div class="page-title">
                <h3>Machine Master</h3>
            </div>

            <!--Mstmachine input field-->
            <div class="row page-section">
                <div class="col-md-4">
                    <div class="input-group">
                        <div class="input-group-prepend prepend-30">
                            <span class="input-group-text">Machine ID</span>
                        </div>
                        <input type="text" class="form-control" placeholder="Machine ID">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group">
                        <div class="input-group-prepend prepend-40">
                            <span class="input-group-text">Run for Model (Part)</span>
                        </div>
                        <input type="text" class="form-control" placeholder="Run for Model (Part)">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group">
                        <div class="input-group-prepend prepend-30">
                            <span class="input-group-text">Machine Status</span>
                        </div>
                        <select class="form-control">
                            <option value="">In Operation</option>
                            <option value="">Maintenance</option>
                            <option value="">Breakdown</option>
                            <option value="">Not In Used</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-12">
                <div class="form-group mb-0">
                    <button class="btn btn-primary mr-2">
                        New
                    </button>
                    <button class="btn btn-primary ml-2 float-right">
                        Search
                    </button>
                </div>
            </div>

            <!--Mstmachine Listing-->
            <div class="row page-section">
                <div class="col-12">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Machine ID</th>
                                <th>Description</th>
                                <th>Run for Model No.</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Row 1 with column 1</td>
                                <td>Row 1 with column 2</td>
                                <td>Row 1 with column 3</td>
                                <td>Row 1 with column 4</td>
                            </tr>
                            <tr>
                                <td>Row 2 with column 1</td>
                                <td>Row 2 with column 2</td>
                                <td>Row 2 with column 3</td>
                                <td>Row 2 with column 4</td>
                            </tr>
                            <tr>
                                <td>Row 3 with column 1</td>
                                <td>Row 3 with column 2</td>
                                <td>Row 3 with column 3</td>
                                <td>Row 3 with column 4</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</body>

</html>