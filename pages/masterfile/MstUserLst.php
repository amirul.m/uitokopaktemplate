<?php include("../../setrelative.php") ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>UserLst</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/package/bootstrap-4.5.3/css/bootstrap.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Source+Code+Pro&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/main.css">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/bootstrap-custom.css">
    <link rel="stylesheet" href="<?php echo RelativePath; ?>/assets/css/custom.css">

</head>

<body>
    <div class="container-fluid">
        <div class="jumbotron jumbotron-master">
            <!--Title and navigation bar-->
            <div class="page-title">
                <h3>User Master</h3>
            </div>

            <!--Usermst input field-->
            <div class="row page-section">
                <div class="col-md-4">
                    <div class="input-group">
                        <div class="input-group-prepend prepend-30">
                            <span class="input-group-text">User Login</span>
                        </div>
                        <input type="text" class="form-control" placeholder="User Login">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group">
                        <div class="input-group-prepend prepend-30">
                            <span class="input-group-text">User Password</span>
                        </div>
                        <input type="password" class="form-control" placeholder="Password">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group">
                        <div class="input-group-prepend prepend-30">
                            <span class="input-group-text">User Name</span>
                        </div>
                        <input type="text" class="form-control" placeholder="User Name">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group">
                        <div class="input-group-prepend prepend-30">
                            <span class="input-group-text">User Role</span>
                        </div>
                        <select class="form-control">
                            <option value="">Select option</option>
                            <option value="">Option 1</option>
                            <option value="">Option 2</option>
                            <option value="">Option 3</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="input-group">
                        <div class="input-group-prepend prepend-30">
                            <span class="input-group-text">Active</span>
                        </div>
                        <select class="form-control">
                            <option value="">All</option>
                            <option value="">Active</option>
                            <option value="">Inactive</option>
                        </select>
                    </div>
                </div>

                <div class="col-12">
                    <div class="form-group mb-0">
                        <button class="btn btn-primary mr-2">
                            New
                        </button>
                        <button class="btn btn-primary ml-2 float-right">
                            Search
                        </button>
                    </div>
                </div>
            </div>


            <!--Usermst Listing-->
            <div class="row page-section">
                <div class="col-12">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>User Login</th>
                                <th>User Name</th>
                                <th>Role</th>
                                <th>Active</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Row 1 with column 1</td>
                                <td>Row 1 with column 2</td>
                                <td>Row 1 with column 3</td>
                                <td>Row 1 with column 4</td>
                            </tr>
                            <tr>
                                <td>Row 2 with column 1</td>
                                <td>Row 2 with column 2</td>
                                <td>Row 2 with column 3</td>
                                <td>Row 2 with column 4</td>
                            </tr>
                            <tr>
                                <td>Row 3 with column 1</td>
                                <td>Row 3 with column 2</td>
                                <td>Row 3 with column 3</td>
                                <td>Row 3 with column 4</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</body>

</html>