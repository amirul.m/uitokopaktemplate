<?php

if (file_exists("index.php")) {
	define("RelativePath", ".");
} else if(file_exists("../index.php")){
	define("RelativePath", "..");
} else if(file_exists("../../index.php")){
	define("RelativePath", "../..");
} else if(file_exists("../../../index.php")){
	define("RelativePath", "../../..");
} else if(file_exists("../../../../index.php")){
	define("RelativePath", "../../../..");
} else if(file_exists("../../../../../index.php")){
	define("RelativePath", "../../../../..");
}

?>