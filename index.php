<?php include("setrelative.php") ?>

<?php

$pages_dir = RelativePath."/pages/guideline";
$pages_arr = scandir($pages_dir);
array_splice($pages_arr, 0, 2);

$dashboards_dir = RelativePath."/pages/dashboards";
$dashboards_arr = scandir($dashboards_dir);
array_splice($dashboards_arr, 0, 2);

$masterfiles_dir = RelativePath."/pages/masterfile";
$masterfiles_arr = scandir($masterfiles_dir);
array_splice($masterfiles_arr, 0, 2);

$experiments_dir = RelativePath."/pages/experiment";
$experiments_arr = scandir($experiments_dir);
array_splice($experiments_arr, 0, 2);

$others_dir = RelativePath."/pages/other";
$others_arr = scandir($others_dir);
array_splice($others_arr, 0, 2);

$scanner_dir = RelativePath."/pages/scanner";
$scanner_arr = scandir($scanner_dir);
array_splice($scanner_arr, 0, 2);

$mould_dir = RelativePath."/pages/mould";
$mould_arr = scandir($mould_dir);
array_splice($mould_arr, 0, 2);

?>
    
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Main Page</title>

    <?php include(RelativePath."/include_packagecss.php") ?>
    <?php include(RelativePath."/include_assetscss.php") ?>
</head>
<body>
    <div class="container my-4">
        <div class="jumbotron">
            <div class="accordion" id="accordionIndex">
                <h1 class="display-4">UI Tokopak Template</h1>
                <div class="card">
                    <div class="card-header" id="headingOne">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                            <p class="m-0">This is just a guidance for developer to create new page using template given.</p>
                        </button>
                    </h2>
                    </div>
                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionIndex">
                    <div class="card-body">
                        <?php if(count($pages_arr) > 0) foreach($pages_arr as $page) { $dir = $pages_dir."/".$page; ?>
                        <a class="btn btn-primary btn-sm" href="<?php echo $dir ?>" role="button"><?php echo ucwords(substr(pathinfo($page, PATHINFO_FILENAME), 3)); ?></a>
                        <?php } ?>
                    </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            <p class="m-0">As for dashboard and login page, may refer as following:</p>
                        </button>
                    </h2>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionIndex">
                    <div class="card-body">
                        <?php if(count($dashboards_arr) > 0) foreach($dashboards_arr as $dashboard) { $dir = $dashboards_dir."/".$dashboard; ?>
                        <a class="btn btn-primary btn-sm" href="<?php echo $dir ?>" target="_blank" role="button"><?php echo ucwords(substr(pathinfo($dashboard, PATHINFO_FILENAME), 3)); ?></a>
                        <?php } ?>
                    </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            <p class="m-0">As for master file, may refer as following:</p>
                        </button>
                    </h2>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionIndex">
                    <div class="card-body">
                        <?php if(count($masterfiles_arr) > 0) foreach($masterfiles_arr as $masterfile) { $dir = $masterfiles_dir."/".$masterfile; ?>
                        <a class="btn-opener btn btn-primary btn-sm mb-1" href="<?php echo $dir ?>" role="button"><?php echo ucwords(pathinfo($masterfile, PATHINFO_FILENAME)); ?></a>
                        <?php } ?>
                    </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                            <p class="m-0">As for other pages, may refer as following:</p>
                        </button>
                    </h2>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionIndex">
                    <div class="card-body">
                        <?php if(count($others_arr) > 0) foreach($others_arr as $other) { $dir = $others_dir."/".$other; ?>
                        <a class="btn-opener btn btn-primary btn-sm mb-1" href="<?php echo $dir ?>" role="button"><?php echo ucwords(pathinfo($other, PATHINFO_FILENAME)); ?></a>
                        <?php } ?>
                    </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFive">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                            <p class="m-0">As for experiment, may refer as following:</p>
                        </button>
                    </h2>
                    </div>
                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionIndex">
                    <div class="card-body">
                        <?php if(count($experiments_arr) > 0) foreach($experiments_arr as $experiment) { $dir = $experiments_dir."/".$experiment; ?>
                        <a class="btn-opener btn btn-primary btn-sm mb-1" href="<?php echo $dir ?>" role="button"><?php echo ucwords(pathinfo($experiment, PATHINFO_FILENAME)); ?></a>
                        <?php } ?>
                    </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSix">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                            <p class="m-0">As for scanner, may refer as following:</p>
                        </button>
                    </h2>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionIndex">
                    <div class="card-body">
                        <?php if(count($scanner_arr) > 0) foreach($scanner_arr as $scanner) { $dir = $scanner_dir."/".$scanner; ?>
                        <a class="btn-opener btn btn-primary btn-sm mb-1" href="<?php echo $dir ?>" role="button"><?php echo ucwords(pathinfo($scanner, PATHINFO_FILENAME)); ?></a>
                        <?php } ?>
                    </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSix">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                            <p class="m-0">As for mould, may refer as following:</p>
                        </button>
                    </h2>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordionIndex">
                    <div class="card-body">
                        <?php if(count($mould_arr) > 0) foreach($mould_arr as $mould) { $dir = $mould_dir."/".$mould; ?>
                        <a class="btn-opener btn btn-primary btn-sm mb-1" href="<?php echo $dir ?>" role="button"><?php echo ucwords(pathinfo($mould, PATHINFO_FILENAME)); ?></a>
                        <?php } ?>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <?php include(RelativePath."/include_packagejs.php") ?>
    <?php include(RelativePath."/include_assetsjs.php") ?>
</body>
</html>