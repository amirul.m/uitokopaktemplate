$(document).ready(function() {
    $('input[name="menu-main[]"]').on('change', function() {
        $('input[name="menu-main[]"]').not(this).prop('checked', false);
        loopUntickCheckbox(this);
        $('.submenu-title-checkbox').each(function() {
            if($(this).is(":checked")) {
                $(this).prop('checked', false);
            }
        });
    });

    $('.submenu-title-checkbox').on('change', function() {
        $('input[name="' + this.name + '"]').not(this).prop('checked', false);
        loopUntickCheckbox(this);
    });

    $('#menu-option').on('change', function() {
        if($(this).is(":checked")) {
            $("label[for='menu-option'] i").removeClass("fa-bars");
            $("label[for='menu-option'] i").addClass("fa-times");
        } else {
            $('input[name="menu-main[]').each(function() {
                if($(this).is(":checked")) {
                    $(this).prop('checked', false);
                }
            });
            $('.submenu-title-checkbox').each(function() {
                if($(this).is(":checked")) {
                    $(this).prop('checked', false);
                }
            });
            $("label[for='menu-option'] i").removeClass("fa-times");
            $("label[for='menu-option'] i").addClass("fa-bars");
        }
    });

    function loopUntickCheckbox(obj) {
        var el = $(obj).next().find('input[type=checkbox]');

        if(el.length > 0) {
            el.each(function() {
                if($(this).is(":checked")) {
                    $(this).prop('checked', false);
                }
                loopUntickCheckbox(this);
            })
        }
    }
});