function notify(message, type) {
    $.notify(
        message, 
        {
            globalPosition: "top right",
            className: type
        }
    );
}

/*
function copyText(id, message) {
	if($(id).val() != "") {
		$(id).focus();
		$(id).select();
		document.execCommand("copy");
		$(id).blur();
		notify(message, "success");
	} else {
		notify("Input is empty", "error");
	}
}
*/