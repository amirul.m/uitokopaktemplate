$(document).ready(function() {
    var textarea_el = $("#code-snippet textarea");
    textarea_el.each(function(index, el) {
        var value = $(this).val();
        var numberOfLineBreaks = (value.match(/\n/g) || []).length;
        var newHeight = 20 + numberOfLineBreaks * 20 + 12 + 2;
        if(numberOfLineBreaks < 2) {
            $(this).attr("rows", 2);
            $(this).next().next().hide();
        } else if(numberOfLineBreaks < 15) {
            $(this).attr("rows", numberOfLineBreaks + 1);
            $(this).next().next().hide();
        } else
            $(this).attr("rows", 5);
        $(this).attr("readonly", "true");
    });
    
    $("#code-snippet #button-copy").on("click", function() {
        var textarea_el = $(this).prev();
        var textarea_el_value =  textarea_el.val();
        if(textarea_el_value != "") {
            textarea_el.focus();
            textarea_el.select();
            document.execCommand("copy");
            textarea_el.val("");
            textarea_el.val(textarea_el_value);
            notify("Succesful copy", "success");
        } else {
            notify("Input is empty", "error");
        }
    });
    
    $("#code-snippet #button-toggle").on("click", function() {
        var textarea_el = $(this).prev().prev();
        var textarea_el_value =  textarea_el.val();
        if(textarea_el_value != "") {
            if(textarea_el.attr("rows") <= 5) {
                var numberOfLineBreaks = (textarea_el_value.match(/\n/g) || []).length;
                var newHeight = 20 + numberOfLineBreaks * 20 + 12 + 2;
                textarea_el.attr("rows", numberOfLineBreaks + 1);
            } else {
                textarea_el.attr("rows", 5);
            }
        } else {
            notify("Input is empty", "error");
        }
    });

    $(".btn-opener").on("click", function(e) {
        e.preventDefault();

        var link = $(this).attr("href");

        window.open(link, "_blank", "width=auto,height=auto,screenX=0,screenY=0,titlebar=yeso");
    })
});